<?php
//activamos almacenamiento en el buffer
ob_start();
session_start();
$rand = rand();
if (!isset($_SESSION['nombre'])) {
	header("Location: login.html");
} else {
	require 'header.php';
	if ($_SESSION['acceso'] == 1) {

		function form_select_months($name, $selected = NULL)
		{
			$html = '';
			$meses  = array('', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
			for ($i = 1; $i <= 12; $i++) {
				if ($selected != $i) {
					$html .= '<option value="' . $i . '">' . $meses[$i] . '</option>';
				} else {
					$html .= '<option value="' . $i . '" selected="selected">' . $meses[$i] . '</option>';
				}
			}
			return $html;
		}
?>
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">

				<!-- Default box -->
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h1 class="box-title">Lista de Cobros <button class="btn btn-sm btn-success" id="btnagregar" data-toggle="modal" data-target="#modal-default"><i class="fa fa-plus-circle"></i>Agregar</button></h1>
								<div class="box-tools pull-right">
								</div>
							</div>
							<!--box-header-->
							<!--centro-->
							<div class="panel-body table-responsive">
								<table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
									<thead>
										<th style="width: 90px;">Opciones</th>
										<th>Fecha</th>
										<th>Grupo</th>
										<th>Alumno</th>
										<th>Teléfono</th>
										<th>Descripción</th>
										<th>Importe</th>
										<th>Método</th>
										<th>Estado</th>
									</thead>
									<tbody>
									</tbody>
									<tfoot>
										<th>Opciones</th>
										<th>Fecha</th>
										<th>Grupo</th>
										<th>Alumno</th>
										<th>Teléfono</th>
										<th>Descripción</th>
										<th>Importe</th>
										<th>Método</th>
										<th>Estado</th>
									</tfoot>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- /.box -->
				<!-- Add charge -->
				<div class="modal fade in modal-default" data-backdrop="static" data-keyboard="false" id="modal-default">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">×</span></button>
								<h4 class="modal-title">Realizar Cobro</h4>
							</div>
							<form action="" name="cash_register" id="cash_register" method="post">
								<input type="hidden" name="id" id="id" value="0">
								<input type="hidden" name="days" id="days">
								<input type="hidden" name="price" id="price">
								<input type="hidden" name="student_id" id="student_id">
								<input type="hidden" name="group_id" id="group_id">
								<div class="modal-body">
									<div class="form-group">
										<label for="group">Grupo:</label>
										<select name="group" id="group" class="form-control select2" style="width: 100%;" required>
										</select>
									</div>
									<div class="form-group">
										<label for="students">Alumno:</label>
										<select name="students" id="students" class="form-control select2" style="width: 100%;" required>
										</select>
										<div id="help-alumn" class="help-block"></div>
									</div>
									<div class="form-group">
										<label for="type">Tipo Cobro:</label>
										<select name="type" id="type" class="form-control" required>
											<option value="" disabled selected>Selecciona un tipo de pago</option>
											<option value="M">Mensualidad</option>
											<option value="I">Inscripción</option>
										</select>
									</div>
									<div id="monthly_payment" class="form-group">
										<label for="months">Mensualidad:</label>
										<select name="months[]" id="months" class="form-control" style="width: 100%;" multiple>
											<?= form_select_months('months') ?>
										</select>
										<div id="help_month" class="help-block"></div>
									</div>
									<div class="form-group">
										<label for="amount">Importe:</label>
										<input class="form-control" type="number" name="amount" id="amount" min="0" step="any" value="0" readonly>
									</div>
								</div>
								<div class="modal-footer">
									<input type="hidden" name="description" id="description" value="Pago de Mensualidad">
									<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
									<button type="button" name="btn-store" id="btn-store" class="btn btn-primary btn-store"><i class="fa fa-dollar"></i> Cobrar</button>
								</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>

			</section>
			<!-- /.content -->
		</div>
	<?php
	} else {
		require 'noacceso.php';
	}
	require 'footer.php';
	?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.20/lodash.min.js" integrity="sha512-90vH1Z83AJY9DmlWa8WkjkV79yfS2n2Oxhsi2dZbIv0nC4E6m5AbH8Nh156kkM7JePmqD6tcZsfad1ueoaovww==" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script src="scripts/payments.js?v=<?= $rand ?>"></script>
	<script>
		



		$('.btn-edit-payment').on('click', function(e) {
			alert('editar');
		});
	</script>
<?php
}
ob_end_flush();
?>