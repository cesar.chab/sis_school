<?php 
ob_start();
session_start();
if (!isset($_SESSION['nombre'])) {
	header("Location: login.html");
} else {
    require_once "../modelos/Alumnos.php";
    $alumno = new Alumnos();
    $result = $alumno->mostrar($_REQUEST['id']);
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SIS | SCHOOL</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="../public/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../public/css/font-awesome.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../public/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="../public/css/_all-skins.min.css">
    <link rel="apple-touch-icon" href="../public/img/apple-touch-icon.png">
    <link rel="shortcut icon" href="../public/img/favicon.ico">

    <!-- DATATABLES -->
    <link rel="stylesheet" type="text/css" href="../public/datatables/jquery.dataTables.min.css">
    <link href="../public/datatables/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="../public/datatables/responsive.dataTables.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="../public/css/bootstrap-select.min.css">
    <link href="../public/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="../public/plugins/sweetalert/sweetalert2.css" rel="stylesheet" />
    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>

<body>
    <div class="wrapper">
        <!-- Main content -->
        <section class="invoice">
            <!-- title row -->
            <div class="row invoice-info">
                <div class="col-xs-12">
                    <div class="col-sm-4 invoice-col">
                        <img src="../public/img/logoTicket.JPG" alt="Logo" height="120">
                    </div>
                    <div class="col-md-4 pull-left">
                        <p class="text-center text-bold">
                            SOLICITUD DE INSCRIPCIÓN<br>
                            AV. JUAN OSEGUERA 789<br>
                            JARDINES DEL CHAMIZAL<br>
                            TECOMAN, COL
                        </p>
                    </div>
                    <div class="col-md-4 pull-right">
                        <strong>Fecha </strong> <?= date('Y-m-d H:i:s') ?>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <hr>

            <div class="row invoice-info">
                <div class="col-md-12">
                    <!-- Box Comment -->
                    <div class="box box-widget">
                        <div class="box-header">
                            <h3 class="box-title">DATOS DEL ALUMNO</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body_">
                            <table class="table" style="width: 100%;" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <label>Nombres</label>
                                        <label class="form-control input-sm">C<?=$result['name'] ?></label>
                                    </td>
                                    <td>
                                        <label>Apellidos:</label>
                                        <label class="form-control input-sm"><?=$result['lastname']?></label>
                                    </td>
                                    <td>
                                        <label>Correo:</label>
                                        <label class="form-control input-sm"><?=$result['email']?></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="">Teléfono Movil:</label>
                                        <label class="form-control input-sm"><?=$result['phone']?></label>
                                    </td>
                                    <td>
                                        <label for="">Teléfono Fijo:</label>
                                        <label class="form-control input-sm"><?=$result['landline']?></label>
                                    </td>
                                    <td>
                                        <label for="">Edad:</label>
                                        <label class="form-control input-sm"><?=$result['age']?></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="">
                                            Tiene alguna discapacidad permanente<br>
                                            <input type="radio" name="isDisability" id="is_disability" class="isDisability" <?=($result['is_disability'] == 1) ? 'checked': ''?>> Si
                                            &nbsp;&nbsp;
                                            <input type="radio" name="isDisability" id="no_disability" class="isDisability" <?=($result['is_disability'] == 0) ? 'checked': ''?>> No
                                        </label>
                                    </td>
                                    <td colspan="2">
                                        <label for="">Si, Cual ?</label>
                                        <label class="form-control input-sm"><?=$result['describe_disability']?></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="">
                                            Tiene alguna leción reciente<br>
                                            <input type="radio" name="recent_injury" id="is_recente_injury" class="recent_injury" <?=($result['recent_injury'] == 1) ? 'checked' : ''?>> Si
                                            &nbsp;&nbsp;
                                            <input type="radio" name="recent_injury" id="no_recente_injury" class="recent_injury" <?=($result['recent_injury'] == 0) ? 'checked' : ''?>> No
                                        </label>
                                    </td>
                                    <td colspan="2">
                                        <label for="">Si, Especifique cuando se lesiono y de que</label>
                                        <label class="form-control input-sm"><?=$result['describe_injury']?></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label class="">
                                            Su estado de salud es?<br>
                                            <input type="radio" name="health_condition" id="good_health_condition" class="health_condition" <?=($result['health_condition'] == 1) ? 'checked' : ''?>> Bueno
                                            &nbsp;&nbsp;
                                            <input type="radio" name="health_condition" id="regular_health_condition" class="health_condition" value="2" <?=($result['health_condition'] == 2) ? 'checked' : ''?>> Regular
                                            &nbsp;&nbsp;<br>
                                            <input type="radio" name="health_condition" id="disease_health_condition" class="health_condition" value="3" <?=($result['health_condition'] == 3) ? 'checked' : ''?>> Presenta una enfermedad
                                        </label>
                                    </td>
                                    <td colspan="2">
                                        <label for="">De que esta enfermo ?</label>
                                        <label class="form-control input-sm"><?=$result['describe_health_condition']?></label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <!-- Box Comment -->
                    <div class="box box-widget">
                        <div class="box-header">
                            <h3 class="box-title">DATOS DEL PADRE/MADRE/TUTOR</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body_">
                            <table class="table">
                                <tr>
                                    <td>
                                        <label>Nombres</label>
                                        <label class="form-control input-sm"><?=$result['tutor_name']?></label>
                                    </td>
                                    <td>
                                        <label>Apellidos:</label>
                                        <label class="form-control input-sm"><?=$result['tutor_last_name']?></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="">Domicilio calle:</label>
                                        <label class="form-control input-sm"><?=$result['tutor_address']?></label>
                                    </td>
                                    <td>
                                        <label for="">Colonia:</label>
                                        <label class="form-control input-sm"><?=$result['suburb']?></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="">Ciudad:</label>
                                        <label class="form-control input-sm"><?=$result['city']?></label>
                                    </td>
                                    <td>
                                        <label for="">Municipio:</label>
                                        <label class="form-control input-sm"><?=$result['municipality']?></label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label for="">Teléfono movil:</label>
                                        <label class="form-control input-sm"><?=$result['tutor_phone']?></label>
                                    </td>
                                    <td>
                                        <label for="">Teléfono fijo:</label>
                                        <label class="form-control input-sm"><?=$result['tutor_landline']?></label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    <!-- Box Comment -->
                    <div class="box box-widget">
                        <div class="box-header">
                            <h3 class="box-title">¿QUE NIVEL DE NATACIÓN TIENE MI HIJO(A)?</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <table class="table">
                                <tr>
                                    <td>
                                        <label>NIVEL 0 MARCAR</label><br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="swimming_level_0_1" id="swimming_level_0_1" <?=($result['swimming_level_0_1'] == 1) ? 'checked':''?>>&nbsp;&nbsp;No sabe nadar y tiene miedo al agua
                                        <!-- </label> -->
                                        <br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="swimming_level_0_2" id="swimming_level_0_2" <?=($result['swimming_level_0_2'] == 1) ? 'checked':''?>>&nbsp;&nbsp;No sabe nadar pero no tiene miedo al agua
                                        <!-- </label> -->
                                    </td>
                                    <td>
                                        <label>NIVEL 1 MARCAR</label><br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="swimming_level_1" id="swimming_level_1" <?=($result['swimming_level_1'] == 1) ? 'checked':''?>>&nbsp;&nbsp;Nada con ayuda de material (Flotadores, espaguetis, tabla)
                                        <!-- </label> -->
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>NIVEL 2 MARCAR</label><br>
                                        <input type="checkbox" name="swimming_level_2" id="swimming_level_2" <?=($result['swimming_level_2'] == 1) ? 'checked':''?>>&nbsp;&nbsp;Nada de crol, pecho, espalda o mariposa
                                    </td>
                                    <td>
                                        <label>NIVEL SOLO DEPORTE</label><br>
                                        <input type="checkbox" name="level_sport_instructor" id="level_sport_instructor"  <?=($result['level_sport_instructor'] == 1) ? 'checked':''?>>&nbsp;&nbsp;Requiere de instructor<br>
                                        <input type="checkbox" name="level_sport_no_instructor" id="level_sport_no_instructor" <?=($result['level_sport_no_instructor'] == 1) ? 'checked':''?>>&nbsp;&nbsp;Solo requiere un horario especifico para ejercitarce solo
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label>
                                            NIVEL REHABILITACIÓN
                                        </label><br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="requires_instructor_help" id="requires_instructor_help" <?=($result['requires_instructor_help'] == 1) ? 'checked':''?>>&nbsp;&nbsp;Requiere ayuda de instructor
                                        <!-- </label> -->
                                        <br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="requires_only_supervision" id="requires_only_supervision" <?=($result['requires_only_supervision'] == 1) ? 'checked':''?>>&nbsp;&nbsp;Requiere solo supervisión
                                        <!-- </label> -->
                                        <br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="type_rehabilitation" id="type_rehabilitation" <?=($result['type_rehabilitation'] == 1) ? 'checked':''?>>&nbsp;&nbsp;Que tipo de rehabilitación requiere (Agregar informe médico)
                                        <!-- </label> --><br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="medical_report" id="medical_report" <?=($result['medical_report'] == 1) ? 'checked':''?>>&nbsp;&nbsp;Entregado (informe médico)
                                        <!-- </label> -->
                                    </td>
                                    <td>
                                        <label>HORARIO</label><br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="monday_schedule" id="monday_schedule" <?=!empty($result['monday_schedule']) ? 'checked':''?>>&nbsp;&nbsp;LUNES
                                         <?=!empty($result['monday_time']) ? ' A LAS ' . $result['monday_time'] : '' ?> 
                                        <!-- </label> --><br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="tuesday_schedule" id="tuesday_schedule" <?=!empty($result['tuesday_schedule']) ? 'checked':''?>>&nbsp;&nbsp;MARTES
                                        <?=!empty($result['tuesday_time']) ? ' A LAS ' . $result['tuesday_time'] : '' ?> 
                                        <!-- </label> --><br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="wednesday_schedule" id="wednesday_schedule" <?=!empty($result['wednesday_schedule']) ? 'checked':''?>>&nbsp;&nbsp;MIERCOLES
                                        <?=!empty($result['wednesday_time']) ? ' A LAS ' . $result['wednesday_time'] : '' ?> 
                                        <!-- </label> --><br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="thursday_schedule" id="thursday_schedule" <?=!empty($result['thursday_schedule']) ? 'checked':''?>>&nbsp;&nbsp;JUEVES
                                        <?=!empty($result['thursday_time']) ? ' A LAS ' . $result['thursday_time'] : '' ?> 
                                        <!-- </label> --><br>
                                        <!-- <label> -->
                                        <input type="checkbox" name="friday_schedule" id="friday_schedule" <?=!empty($result['friday_schedule']) ? 'checked':''?>>&nbsp;&nbsp;VIERNES
                                        <?=!empty($result['friday_time']) ? ' A LAS ' . $result['friday_time'] : '' ?> <br>
                                        <!-- </label> -->
                                        <input type="checkbox" name="saturday_schedule" id="saturday_schedule" <?=!empty($result['saturday_schedule']) ? 'checked':''?>>&nbsp;&nbsp;SÁBADO
                                        <?=!empty($result['saturday_time']) ? ' A LAS ' . $result['saturday_time'] : '' ?> 
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <div class="row">
                <h5 class="text-center"><strong>OBSERVACIONES IMPORTANTES</strong></h5>
                <ul style="font-size: 10px;">
                    <li>Esta solicitud debera ser firmada en caso de menores de edad por el padre, madre o tutor</li>
                    <li>Para garantizar su lugar es necesario realizar su pago de inscripcion y la primer mensualidad</li>
                    <li>El plazo para pagar será durante la primer semana del mes</li>
                    <li>De no cubrir su mensualidad podria perder su plaza</li>
                    <li>Padra darse de baja debera avisar al encargado una semana antes de que finalice el mes</li>
                    <li>El Solicitante padre, madre, tutor deberá presentar un certificado médico, en el cual declara que el alumno(a) no padece de enfermedad infecto contagiosa,
                        ni defecto físico alguno que impida la práctica de la natación.
                    </li>
                    <li>SU PAGO PODRA SER EN EFECTIVO O POR DEPOSITO DE CUENTA</li>
                </ul><br>
                <h5 class="text-center"><strong>CONDICIONES DE INSCRIPCIÓN</strong></h5>
                <ul style="font-size: 10px;">
                    <li>El solicitante manifiesta conocer las condiciones y normas de uso de las instalaciones.</li>
                    <li>Es obligatorio el uso de gorro de natación, y traje de baño y una vez afuera de la alberca debera usar calzado adecuado antiderrapante </li>
                    <li>El club no se hace responsable de la pérdida de prendas y objetos de valor extraviados</li>
                    <li>La cobertura del riesgo de accidentes derivado de la práctica en estas actividades no lleva implicito tener un seguro de accidentes,
                        corriendo a cargo del solicitante los gastos médicos por lesión de accidente a través de la Seguridad Social o Seguros Privados.
                    </li>
                    <li>El solicitante firma bajo protesta de decir verdad y en aceptación de todas las condiciones y reglas expuestas</li>
                </ul>
            </div>
            <!-- /.row -->
            <div class="row">
                <br><br>
                <div class="col-md-2"></div>
                <div class="col-md-6">
                    <p class="pull-left text-center">
                        ________________________________<br>
                        <span class="text-center text-bold">Nombre y firma</span>
                    </p>
                    <p class="pull-right text-center">
                        ________________________________<br>
                        <span class="text-center text-bold">Fecha</span>
                    </p>
                </div>
                <div class="col-md-2"></div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- ./wrapper -->
    <script type="text/javascript">
        window.addEventListener("load", window.print());
    </script>
</body>

</html>