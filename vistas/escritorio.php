<?php
//activamos almacenamiento en el buffer
ob_start();
session_start();
if (!isset($_SESSION['nombre'])) {
	header("Location: login.html");
} else {

	if ($_SESSION['escritorio'] == 1) {
		$user_id = $_SESSION["idusuario"];
		require_once "../modelos/Consultas.php";
		$consulta = new Consultas();
		$rsptav = $consulta->cantidadalumnos($user_id);
		$schedules = $consulta->getAllSchedules();
		$students = $consulta->getAllStudents();
		// echo json_encode($students);
		// exit();
		$regv = $rsptav->fetch_object();
		$totalestudiantes = $regv->total_alumnos;
		$cap_almacen = 3000;
		require 'header.php';
?>
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">
				<!-- Default box -->
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="panel-body">
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><i class="fa fa-clock-o"></i> Horarios</a></li>
										<li><a href="#tab_2" data-toggle="tab" aria-expanded="false"><i class="fa fa-users"></i> Grupos</a></li>
										<li><a href="#tab_3" data-toggle="tab" aria-expanded="false"><i class="fa fa-dollar"></i> Pagos Pedientes</a></li>
									</ul>
									<div class="tab-content">
										<div class="tab-pane active" id="tab_1">
											<div class="row">
												<div class="col-md-12">
													<div class="box box-primary">
														<div class="box-body no-padding">
															<!-- THE CALENDAR -->
															<div id="calendar"></div>
														</div>
														<!-- /.box-body -->
													</div>
													<!-- /. box -->
												</div>
												<!-- /.col -->
											</div>
											<!-- /.row -->
										</div>
										<!-- /.tab-pane -->
										<div class="tab-pane" id="tab_2">
											<div class="row">
												<div class="col-md-12">
													<?php $rspta = $consulta->cantidadgrupos($user_id);
													$colores = array("box box-success direct-chat direct-chat-success bg-green", "box box-primary direct-chat direct-chat-primary bg-aqua", "box box-warning direct-chat direct-chat-warning bg-yellow", "box box-danger direct-chat direct-chat-danger bg-red");
													while ($reg = $rspta->fetch_object()) {
														$idgrupo = $reg->idgrupo;
														$nombre_grupo = $reg->nombre;
													?>
														<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
															<!-- DIRECT CHAT SUCCESS -->
															<div class="<?php echo $colores[array_rand($colores)]; ?> collapsed-box">
																<div class="box-header with-border">
																	<h3 class="box-title"><?php echo $nombre_grupo; ?></h3>
																	<div class="box-tools pull-right">
																		<span data-toggle="tooltip" title="" class="badge" data-original-title="Cantidad de Estudiantes">
																			<?php
																			$rsptag = $consulta->cantidadg($user_id, $idgrupo);
																			while ($regrupo = $rsptag->fetch_object()) {
																				echo $regrupo->total_alumnos;
																			}
																			?>
																		</span>
																		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
																		</button>
																	</div>
																</div>
																<!-- /.box-header -->
																<div class="box-body">
																	<!-- Conversations are loaded here -->
																	<div class="direct-chat-messages">
																		<!-- Message. Default to the left -->
																		<div class="direct-chat-msg">
																			<?php
																			$rsptas = $consulta->cantidadalumnos_porgrupo($user_id, $idgrupo);
																			while ($reg = $rsptas->fetch_object()) {

																				if (empty($reg->image)) {
																					echo ' <img class="img-circle" src="../files/articulos/anonymous.png" height="50px" width="50px">';
																				} else echo '<img class="img-circle" src="../files/articulos/' . $reg->image . '" height="50px" width="50px">';
																			} ?>
																			<!-- /.direct-chat-text -->
																		</div>
																	</div>
																	<!-- /.direct-chat-pane -->
																</div>
																<!-- /.box-body -->
																<div class="box-footer">
																	<a href="vista_grupo.php?idgrupo=<?php echo $idgrupo; ?>" class="btn btn-default form-control">Ir... <i class="fa fa-arrow-circle-right"></i></a>
																</div>
																<!-- /.box-footer-->
															</div>
															<!--/.direct-chat -->
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
										<!-- /.tab-pane -->
										<div class="tab-pane" id="tab_3">
											<div class="row">
												<div class="col-md-12">
													<div class="box">
														<div class="box-header">
															<h3 class="box-title">Pagos pendientes</h3>
														</div>
														<!-- /.box-header -->
														<div class="box-body">
															<div class="dataTables_wrapper form-inline dt-bootstrap">
																<table id="example1" class="table table-bordered table-striped">
																	<thead>
																		<tr>
																			<th>Grupo</th>
																			<th>Alumno</th>
																			<th>Edad</th>
																			<th>Correo</th>
																			<th>Teléfono</th>
																		</tr>
																	</thead>
																	<tbody>

																	</tbody>
																</table>
															</div>
														</div>
														<!-- /.box-body -->
													</div>
												</div>
											</div>
										</div>
									</div>
									<!-- /.tab-content -->
								</div>
							</div>
							<!--fin centro-->
						</div>
					</div>
				</div>
				<!-- /.box -->
				<div class="modal fade" id="modal-default">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span></button>
								<h4 id="modal-title" class="modal-title">Detalle</h4>
							</div>
							<div id="modal-detail" class="modal-body">

							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cerrar</button>
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->

			</section>
			<!-- /.content -->
		</div>
	<?php
	} else {
		require 'noacceso.php';
	}

	require 'footer.php';
	?>
	<script>
		$(function() {

			let students = '<?= json_encode($students); ?>';
			let studentsParse = []
			if (students != undefined) {
				studentsParse = JSON.parse(students);
			}

			var date = new Date()
			// var d = date.getDate(),
			// 	m = date.getMonth(),
			// 	y = date.getFullYear()
			$('#calendar').fullCalendar({
				lang: "es",
				displayEventTime: false,
				defaultView: "agendaDay",
				defaultDate: date,
				allDay: false,
				allDaySlot: false,
				slotDuration: "00:40:00",
				slotLabelInterval: "00:40:00",
				minTime: "09:00:00",
				maxTime: "20:00:00",
				nowIndicator: true,
				header: {
					left: 'today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				buttonText: {
					today: 'Hoy',
					month: 'Mes',
					week: 'Semana',
					day: 'Día'
				},
				//Random default events
				events: studentsParse,
				eventRender: function(event, element) {
					info = ` <b>Correo:</b> ${event.email} <b>Teléfono: </b> ${event.phone}`;
					element.find(".fc-title").append(info);
				},
				eventClick: onEventClick,
				viewRender: viewRender,
			});

			function onEventClick(calEvent, jsEvent, view) {
				console.log(calEvent)
				$('#modal-title').html('ALUMNO: ' + calEvent.name);
				$('#modal-detail').append(`
					<p><b>Email: </b> ${calEvent.email}</p>
					<p><b>Teléfono: </b> ${calEvent.phone}</p>
					<p><b>Hora inicio: </b> ${calEvent.start.format("HH:mm:ss")}</p>
					<p><b>Sesión de:</b> 40 Minutos</p>
				`);
				$('#modal-default').modal('show');
			}

			function viewRender(view, element) {
				$("#calendar").fullCalendar("option", "height", 'auto');
				if (view.name == 'agendaDay') {
					$("#calendar").fullCalendar("changeView", "agendaDay")
				}
			}
		});
	</script>

<?php
}

ob_end_flush();
?>