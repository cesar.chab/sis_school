<?php
//activamos almacenamiento en el buffer
ob_start();
session_start();
$rand = rand();
if (!isset($_SESSION['nombre'])) {
    header("Location: login.html");
} else {

    require 'header.php';
    if ($_SESSION['acceso'] == 1) {
        require '../modelos/Report.php';
        $reporte = new Reporte();
        $result = $reporte->listAlumnos();
        $resultSet = array();
        while ($cRecord = $result->fetch_assoc()) {
            $resultSet[] = $cRecord;
        }
?>
        <div class="content-wrapper">
            <!-- Main content -->
            <section class="content">
                <!-- Default box -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Reporte de Alumnos</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="dataTables_wrapper form-inline dt-bootstrap">
                                    <table id="example1" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Grupo</th>
                                                <th>Alumno</th>
                                                <th>Edad</th>
                                                <th>Correo</th>
                                                <th>Teléfono</th>
                                                <!-- <th>Horarios</th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                            $schedules = array();
                                            foreach ($resultSet as $key => $item) {                                                
                                            ?>
                                                <tr>
                                                    <td><?=$item['grupo']?></td>
                                                    <td><?=$item['alumno']?></td>
                                                    <td><?=$item['age']?></td>
                                                    <td><?=$item['email']?></td>
                                                    <td><?=$item['phone']?></td>
                                                    <!-- <td>
                                                        <?php 
                                                        if ($item['monday_schedule'] == 1) {
                                                            echo "Lunes, ";
                                                        } 
                                                        if ($item['tuesday_schedule'] == 1) {
                                                            echo "Martes, ";
                                                        } 
                                                        if ($item['wednesday_schedule'] == 1) {
                                                            echo "Miercoles, ";
                                                        } 
                                                        if ($item['thursday_schedule'] == 1) {
                                                            echo "Jueves, ";
                                                        } 
                                                        if ($item['friday_schedule'] == 1) {
                                                            echo "Viernes ";
                                                        }
                                                        ?>
                                                    </td> -->
                                                </tr>
                                            <?php
                                            } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                    </div>
                </div>
                <!-- /.box -->

            </section>
            <!-- /.content -->
        </div>
    <?php
    } else {
        require 'noacceso.php';
    }
    require 'footer.php';
    ?>
    <script src="scripts/report.js?v=<?= $rand ?>"></script>
    <script>
        $(function() {
            $('#example1').DataTable()
            // $('#example2').DataTable({
            //     'paging': true,
            //     'lengthChange': false,
            //     'searching': false,
            //     'ordering': true,
            //     'info': true,
            //     'autoWidth': false
            // })
        })
    </script>
<?php
}

ob_end_flush();
?>