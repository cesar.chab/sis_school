<?php
/*! 
  @function num2letras () 
  @abstract Dado un número lo devuelve escrito. 
  @param $num number - Número a convertir. 
  @param $fem bool - Forma femenina (true) o no (false). 
  @param $dec bool - Con decimales (true) o no (false). 
  @result string - Devuelve el n?mero escrito en letra. 

*/
function num2letras($num, $fem = false, $dec = true)
{
    $matuni[2]  = "dos";
    $matuni[3]  = "tres";
    $matuni[4]  = "cuatro";
    $matuni[5]  = "cinco";
    $matuni[6]  = "seis";
    $matuni[7]  = "siete";
    $matuni[8]  = "ocho";
    $matuni[9]  = "nueve";
    $matuni[10] = "diez";
    $matuni[11] = "once";
    $matuni[12] = "doce";
    $matuni[13] = "trece";
    $matuni[14] = "catorce";
    $matuni[15] = "quince";
    $matuni[16] = "dieciseis";
    $matuni[17] = "diecisiete";
    $matuni[18] = "dieciocho";
    $matuni[19] = "diecinueve";
    $matuni[20] = "veinte";
    $matunisub[2] = "dos";
    $matunisub[3] = "tres";
    $matunisub[4] = "cuatro";
    $matunisub[5] = "quin";
    $matunisub[6] = "seis";
    $matunisub[7] = "sete";
    $matunisub[8] = "ocho";
    $matunisub[9] = "nove";

    $matdec[2] = "veint";
    $matdec[3] = "treinta";
    $matdec[4] = "cuarenta";
    $matdec[5] = "cincuenta";
    $matdec[6] = "sesenta";
    $matdec[7] = "setenta";
    $matdec[8] = "ochenta";
    $matdec[9] = "noventa";
    $matsub[3]  = 'mill';
    $matsub[5]  = 'bill';
    $matsub[7]  = 'mill';
    $matsub[9]  = 'trill';
    $matsub[11] = 'mill';
    $matsub[13] = 'bill';
    $matsub[15] = 'mill';
    $matmil[4]  = 'millones';
    $matmil[6]  = 'billones';
    $matmil[7]  = 'de billones';
    $matmil[8]  = 'millones de billones';
    $matmil[10] = 'trillones';
    $matmil[11] = 'de trillones';
    $matmil[12] = 'millones de trillones';
    $matmil[13] = 'de trillones';
    $matmil[14] = 'billones de trillones';
    $matmil[15] = 'de billones de trillones';
    $matmil[16] = 'millones de billones de trillones';

    //Zi hack
    $float = explode('.', $num);
    $num = $float[0];

    $num = trim((string)@$num);
    if ($num[0] == '-') {
        $neg = 'menos ';
        $num = substr($num, 1);
    } else
        $neg = '';
    while ($num[0] == '0') $num = substr($num, 1);
    if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num;
    $zeros = true;
    $punt = false;
    $ent = '';
    $fra = '';
    for ($c = 0; $c < strlen($num); $c++) {
        $n = $num[$c];
        if (!(strpos(".,'''", $n) === false)) {
            if ($punt) break;
            else {
                $punt = true;
                continue;
            }
        } elseif (!(strpos('0123456789', $n) === false)) {
            if ($punt) {
                if ($n != '0') $zeros = false;
                $fra .= $n;
            } else

                $ent .= $n;
        } else

            break;
    }
    $ent = '     ' . $ent;
    if ($dec and $fra and !$zeros) {
        $fin = ' coma';
        for ($n = 0; $n < strlen($fra); $n++) {
            if (($s = $fra[$n]) == '0')
                $fin .= ' cero';
            elseif ($s == '1')
                $fin .= $fem ? ' una' : ' un';
            else
                $fin .= ' ' . $matuni[$s];
        }
    } else
        $fin = '';
    if ((int)$ent === 0) return 'Cero ' . $fin;
    $tex = '';
    $sub = 0;
    $mils = 0;
    $neutro = false;
    while (($num = substr($ent, -3)) != '   ') {
        $ent = substr($ent, 0, -3);
        if (++$sub < 3 and $fem) {
            $matuni[1] = 'una';
            $subcent = 'as';
        } else {
            $matuni[1] = $neutro ? 'un' : 'uno';
            $subcent = 'os';
        }
        $t = '';
        $n2 = substr($num, 1);
        if ($n2 == '00') {
        } elseif ($n2 < 21)
            $t = ' ' . $matuni[(int)$n2];
        elseif ($n2 < 30) {
            $n3 = $num[2];
            if ($n3 != 0) $t = 'i' . $matuni[$n3];
            $n2 = $num[1];
            $t = ' ' . $matdec[$n2] . $t;
        } else {
            $n3 = $num[2];
            if ($n3 != 0) $t = ' y ' . $matuni[$n3];
            $n2 = $num[1];
            $t = ' ' . $matdec[$n2] . $t;
        }
        $n = $num[0];
        if ($n == 1) {
            $t = ' ciento' . $t;
        } elseif ($n == 5) {
            $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t;
        } elseif ($n != 0) {
            $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t;
        }
        if ($sub == 1) {
        } elseif (!isset($matsub[$sub])) {
            if ($num == 1) {
                $t = ' mil';
            } elseif ($num > 1) {
                $t .= ' mil';
            }
        } elseif ($num == 1) {
            $t .= ' ' . $matsub[$sub] . '?n';
        } elseif ($num > 1) {
            $t .= ' ' . $matsub[$sub] . 'ones';
        }
        if ($num == '000') $mils++;
        elseif ($mils != 0) {
            if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub];
            $mils = 0;
        }
        $neutro = true;
        $tex = $t . $tex;
    }
    $tex = $neg . substr($tex, 1) . $fin;
    //Zi hack --> return ucfirst($tex);
    if (isset($float[1])) {
        $end_num = ucfirst($tex) . ' pesos ' . $float[1] . '/100 M.N.';
    } else {
        $end_num = ucfirst($tex) . ' pesos 00/100 M.N.';
    }   
    
    return strtoupper($end_num);
}




require_once "../modelos/Payments.php";
require "../fpdf182/fpdf.php";

$months = array(
    "1" => "Enero",
    "2" => "Febrero",
    "3" => "Marzo",
    "4" => "Abril",
    "5" => "Mayo",
    "6" => "Junio",
    "7" => "Julio",
    "8" => "Agosto",
    "9" => "Septiembre",
    "10" => "Octubre",
    "11" => "Noviembre",
    "12" => "Diciembre"
);
$payment = new Payments();
$data = $payment->show($_GET['id']);
$data = json_decode(json_encode($data));
$detail = $payment->show_detail($_GET['id']);

$price = $data->amount;
$quantity = 1;
$payments_months = "";
if (!empty($detail)) {
    $price = floatVal($data->amount) / sizeof($detail);
    $quantity = sizeof($detail);
    foreach ($detail as $key => $i) {
        $payments_months .= $months[$i->month] . ", ";
    }
}
$folio = "TK-" . str_pad($data->id, 5, "0", STR_PAD_LEFT);

$pdf = new FPDF('P', 'mm', array(80, 150)); // Tamaño tickt 80mm x 150 mm (largo aprox)
$pdf->AddPage();
$pdf->Image('../files/logo/logoTicket.JPG', 4, 8, 20);
$pdf->SetFont('Helvetica', '', 12);
$pdf->Cell(65, 4, 'SIS SCHOOL', 0, 1, 'C');
$pdf->SetFont('Helvetica', '', 8);
$pdf->Cell(65, 4, 'AV. JUAN OSEGUERA 789', 0, 1, 'C');
$pdf->Cell(65, 4, 'JARDINES DEL CHAMIZAL', 0, 1, 'C');
$pdf->Cell(65, 4, 'TECOMAN, COL', 0, 1, 'C');
// $pdf->Cell(60,4,'999 888 777',0,1,'C');
// $pdf->Cell(60,4,'alfredo@lacodigoteca.com',0,1,'C');

// DATOS FACTURA        
$pdf->Ln(3);
$pdf->Cell(60, 4, 'Folio : ' . $folio, 0, 1, '');
$pdf->Cell(60, 4, 'Fecha: ' . date('Y-m-d H:i:s', strtotime($data->updated_at)), 0, 1, '');
$pdf->Cell(60, 4, 'Metodo de pago: Efectivo', 0, 1, '');
$pdf->Cell(60, 4, '--------------------------ALUMNO--------------------------', 0, 1, '');
$pdf->Cell(60, 4, 'Nombre : ' . $data->alumno, 0, 1, '');
$pdf->Cell(60, 4, 'Tel : ' . $data->phone, 0, 1, '');
$pdf->Cell(60, 4, 'Correo : ' . $data->email, 0, 1, '');
$pdf->Cell(60, 4, 'Grupo : ' . $data->grupo, 0, 1, '');
$pdf->Cell(60, 4, utf8_decode('Dirección : ') . $data->address, 0, 1, '');

// COLUMNAS
$pdf->SetFont('Helvetica', 'B', 7);
$pdf->Cell(30, 10, utf8_decode('Descripción'), 0);
$pdf->Cell(5, 10, 'Cant.', 0, 0, 'R');
$pdf->Cell(10, 10, 'Precio', 0, 0, 'R');
$pdf->Cell(15, 10, 'Total', 0, 0, 'R');
$pdf->Ln(8);
$pdf->Cell(60, 0, '', 'T');
$pdf->Ln(0);

// PRODUCTOS
$pdf->SetFont('Helvetica', '', 7);
$pdf->MultiCell(30, 5, utf8_decode($data->description), 0, 'L');
$pdf->Cell(32, -4, $quantity, 0, 0, 'R');
$pdf->Cell(15, -4, '$' . number_format(round($price, 2), 2, ',', ','), 0, 0, 'R');
$pdf->Cell(14, -4, '$' . number_format(round($data->amount, 2), 2, ',', ','), 0, 0, 'R');
$pdf->Ln(0);
if (!empty($detail)) {
    $pdf->MultiCell(100, 4, 'Meses pagados: ' . $payments_months, 0, 'L');
}

// SUMATORIO DE LOS PRODUCTOS Y EL IVA
$pdf->Cell(60, 0, '', 'T');
$pdf->Ln(2);
$pdf->Cell(25, 10, 'Subtotal.', 0);
$pdf->Cell(21, 10, '', 0);
$pdf->Cell(15, 10, '$' . number_format(round((round($data->amount, 2)), 2), 2, ',', ','), 0, 0, 'R');
$pdf->Ln(3);
$pdf->Cell(25, 10, 'Total', 0);
$pdf->Cell(20, 10, '', 0);
$pdf->Cell(16, 10, '$' . number_format(round($data->amount, 2), 2, ',', ','), 0, 0, 'R');

// PIE DE PAGINA
$pdf->Ln(10);

$pdf->MultiCell(0, 4, 'RECIBI DE ' . utf8_decode($data->alumno) . ' LA CANTIDAD DE '. number_format(round($data->amount, 2), 2, ',', ',') . ' (' . num2letras($data->amount) . ') POR CONCEPTO DE ' . strtoupper(utf8_decode($data->description)) .'.' , 0, 'L');
$pdf->Ln(5);
$pdf->MultiCell(0, 4, 'FIRMA: __________________________________', 0, 'C');
$pdf->MultiCell(0, 4, $data->usuario, 0, 'C');
$pdf->Output('ticket.pdf', 'i');
