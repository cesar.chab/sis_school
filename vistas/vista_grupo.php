<?php
//activamos almacenamiento en el buffer
ob_start();
session_start();
if (!isset($_SESSION['nombre'])) {
	header("Location: login.html");
} else {

	require 'header.php';
	require '../config/helper.php';
	if ($_SESSION['grupos'] == 1) {

		$idgrupo = $_GET['idgrupo'];

		require_once "../modelos/Grupos.php";
		$grupos = new Grupos();
		$rspta = $grupos->mostrar_grupo($idgrupo);
		$reg = $rspta->fetch_object();
		$nombre_grupo = $reg->nombre;
		$helper = new Helper();


?>
		<div class="content-wrapper">
			<!-- Main content -->
			<section class="content">

				<!-- Default box -->
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border">
								<h1 class="box-title">Grupo: <?php echo $nombre_grupo; ?> <button class="btn btn-success" onclick="mostrarform(true)" id="btnagregar"><i class="fa fa-plus-circle"></i>Agregar Alumno</button> </h1>
								<a id="btnasistencia" href="asistencia.php?idgrupo=<?php echo $idgrupo; ?>" class="btn btn-warning"><i class='fa fa-check'></i> Asistencia</a>
								<a id="btnconducta" href="conducta.php?idgrupo=<?php echo $idgrupo; ?>" class="btn btn-primary"><i class='fa fa-smile-o'></i> Comportamiento</a>
								<a id="btncalificaciones" href="calificaciones.php?idgrupo=<?php echo $idgrupo; ?>" class="btn btn-danger"><i class='fa fa-tasks'></i> Calificaciones</a>
								<a id="btncursos" href="cursos.php?idgrupo=<?php echo $idgrupo; ?>" class="btn btn-primary"><i class='fa fa-th-large'></i> Cursos</a>
								<a id="btnlistas" href="listasis.php?idgrupo=<?php echo $idgrupo; ?>" class="btn btn-info"><i class='fa fa-th-list'></i> Listas</a>
								<div class="box-tools pull-right">
									<a id="btngrupos" href="escritorio.php"><button class="btn btn-info"><i class='fa fa-th-large'></i> Grupos</button></a>
								</div>
							</div>
							<!--box-header-->
							<!--centro-->
							<div class="panel-body table-responsive" id="listadoregistros">
								<table id="tbllistado" class="table table-striped table-bordered table-condensed table-hover">
									<thead>
										<th>Opciones</th>
										<!-- <th>Imagen</th> -->
										<th>Nombre</th>
										<th>Apellidos</th>
										<th>Telefono</th>
										<th>Dirección</th>
										<th>Email</th>
									</thead>
									<tbody>
									</tbody>
									<tfoot>
										<th>Opciones</th>
										<!-- <th>Imagen</th> -->
										<th>Nombre</th>
										<th>Apellidos</th>
										<th>Telefono</th>
										<th>Dirección</th>
										<th>Email</th>
									</tfoot>
								</table>
							</div>
							<div class="panel-body" id="formularioregistros">
								<form action="" name="formulario" id="formulario" method="POST">
									<div class="col-md-12">
										<!-- Box Comment -->
										<div class="box box-widget">
											<div class="box-header with-border">
												<h3 class="box-title_">DATOS DEL ALUMNO</h3>
											</div>
											<!-- /.box-header -->
											<div class="box-body">
												<div class="form-group col-lg-4 col-md-4 col-xs-4">
													<label for="">Nombres(*):</label>
													<input type="hidden" id="idgrupo" name="idgrupo" value="<?php echo $_GET["idgrupo"]; ?>">
													<input type="hidden" name="idalumno" id="idalumno">
													<input class="form-control" type="text" name="nombre" id="nombre" maxlength="100" placeholder="Nombre" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" required>
												</div>
												<div class="form-group col-lg-4 col-md-4 col-xs-4">
													<label for="">Apellidos(*):</label>
													<input class="form-control" type="text" name="apellidos" id="apellidos" maxlength="100" placeholder="Nombre" onKeyUp="document.getElementById(this.id).value=document.getElementById(this.id).value.toUpperCase()" required>
												</div>
												<div class="form-group col-lg-4 col-md-4 col-xs-12">
													<label for="">Correo:</label>
													<input class="form-control" type="email" name="email" id="email" placeholder="Correo">
												</div>
												<div class="form-group col-lg-4 col-md-4 col-xs-12">
													<label for="">Teléfono Movil:</label>
													<input class="form-control" type="tel" name="phone" id="phone" maxlength="10" placeholder="Teléfono movil">
												</div>
												<div class="form-group col-lg-4 col-md-4 col-xs-12">
													<label for="">Teléfono Fijo:</label>
													<input class="form-control" type="tel" name="landline" id="landline" maxlength="10" placeholder="Teléfono fijo">
												</div>
												<div class="form-group col-lg-4 col-md-4 col-xs-12">
													<label for="">Edad:</label>
													<input class="form-control" type="number" name="age" id="age" placeholder="Edad">
												</div>
												<div class="form-group col-lg-4 col-md-4 col-xs-12">
													<label class="">
														Tiene alguna discapacidad permanente<br>
														<input type="radio" name="isDisability" id="is_disability" class="isDisability" value="1"> Si
														&nbsp;&nbsp;
														<input type="radio" name="isDisability" id="no_disability" class="isDisability" value="0"> No
													</label>
												</div>
												<div class="form-group col-lg-8 col-md-8 col-xs-12">
													<label for="">Si, Cual ?</label>
													<input class="form-control" type="text" name="describe_disability" id="describe_disability" placeholder="Describe su discapacidad permanente...">
												</div>
												<div class="form-group col-lg-4 col-md-4 col-xs-12">
													<label class="">
														Tiene alguna leción reciente<br>
														<input type="radio" name="recent_injury" id="is_recente_injury" class="recent_injury" value="1"> Si
														&nbsp;&nbsp;
														<input type="radio" name="recent_injury" id="no_recente_injury" class="recent_injury" value="0"> No
													</label>
												</div>
												<div class="form-group col-lg-8 col-md-8 col-xs-12">
													<label for="">Si, Especifique cuando se lesiono y de que</label>
													<input class="form-control" type="text" name="describe_injury" id="describe_injury" placeholder="Describe su lesion reciente">
												</div>
												<div class="form-group col-lg-4 col-md-4 col-xs-12">
													<label class="">
														Su estado de salud es?<br>
														<input type="radio" name="health_condition" id="good_health_condition" class="health_condition" value="1"> Bueno
														&nbsp;&nbsp;
														<input type="radio" name="health_condition" id="regular_health_condition" class="health_condition" value="2"> Regular
														&nbsp;&nbsp;<br>
														<input type="radio" name="health_condition" id="disease_health_condition" class="health_condition" value="3"> Presenta una enfermedad
													</label>
												</div>
												<div class="form-group col-lg-8 col-md-8 col-xs-12">
													<label for="">De que esta enfermo ?</label>
													<input class="form-control" type="text" name="describe_health_condition" id="describe_health_condition" placeholder="Describe de que esta enfermo">
												</div>

											</div>
											<!-- /.box-body -->
										</div>
										<!-- /.box -->
									</div>
									<div class="col-md-12">
										<!-- Box Comment -->
										<div class="box box-widget">
											<div class="box-header with-border">
												<h3 class="box-title_">DATOS DEL PADRE/MADRE/TUTOR</h3>
											</div>
											<!-- /.box-header -->
											<div class="box-body">
												<div class="form-group col-lg-6 col-md-6 col-xs-12">
													<label for="tutor_name">Nombres:</label>
													<input class="form-control" type="text" name="tutor_name" id="tutor_name" maxlength="100" placeholder="Nombre">
												</div>
												<div class="form-group col-lg-6 col-md-6 col-xs-12">
													<label for="tutor_last_name">Apellidos:</label>
													<input class="form-control" type="text" name="tutor_last_name" id="tutor_last_name" maxlength="100" placeholder="Apellidos">
												</div>
												<div class="form-group col-lg-8 col-md-8 col-xs-12">
													<label for="">Domicilio Calle </label>
													<input class="form-control" type="text" name="address" id="address" placeholder="Dirección">
												</div>
												<div class="form-group col-lg-4 col-md-4 col-xs-12">
													<label for="suburb">Colonia </label>
													<input class="form-control" type="text" name="suburb" id="suburb" placeholder="Colonia">
												</div>
												<div class="form-group col-lg-6 col-md-6 col-xs-12">
													<label for="city">Ciudad </label>
													<input class="form-control" type="text" name="city" id="city" placeholder="Ciudad">
												</div>
												<div class="form-group col-lg-6 col-md-6 col-xs-12">
													<label for="municipality">Municipio </label>
													<input class="form-control" type="text" name="municipality" id="municipality" placeholder="Municipio">
												</div>
												<div class="form-group col-lg-6 col-md-6 col-xs-12">
													<label for="tuto_phone">Teléfono Movil </label>
													<input class="form-control" type="tel" name="tutor_phone" id="tutor_phone" placeholder="Teléfono movil">
												</div>
												<div class="form-group col-lg-6 col-md-6 col-xs-12">
													<label for="tutor_landline">Teléfono Fijo </label>
													<input class="form-control" type="tel" name="tutor_landline" id="tutor_landline" placeholder="Teléfono fijo">
												</div>
											</div>
										</div>
										<!-- /.box -->
									</div>
									<div class="col-md-12">
										<div class="box box-widget">
											<div class="box-header with-border">
												<h3 class="box-title_">¿QUE NIVEL DE NATACIÓN TIENE MI HIJO(A)?</h3>
											</div>
											<!-- Nivel 0/1-->
											<div class="box-body">
												<div class="col-md-6">
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<h3 class="box-title">NIVEL 0 MARCAR</h3>
													</div>
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<label>
															<input type="checkbox" name="swimming_level_0_1" id="swimming_level_0_1" value="0">&nbsp;&nbsp;No sabe nadar y tiene miedo al agua
														</label>
														<label>
															<input type="checkbox" name="swimming_level_0_2" id="swimming_level_0_2" value="0">&nbsp;&nbsp;No sabe nadar pero no tiene miedo al agua
														</label>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<h3 class="box-title">NIVEL 1 MARCAR</h3>
													</div>
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<label>
															<input type="checkbox" name="swimming_level_1" id="swimming_level_1" value="0">&nbsp;&nbsp;Nada con ayuda de material (Flotadores, espaguetis, tabla)
														</label>
													</div>
												</div>
											</div>
											<!-- /.Nivel 0/1-->

											<div class="box-body">
												<div class="col-md-6">
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<h3 class="box-title">NIVEL 2 MARCAR</h3>
													</div>
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<label>
															<input type="checkbox" name="swimming_level_2" id="swimming_level_2" value="0">&nbsp;&nbsp;Nada de crol, pecho, espalda o mariposa
														</label>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<h3 class="box-title">NIVEL SOLO DEPORTE</h3>
													</div>
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<label>
															<input type="checkbox" name="level_sport_instructor" id="level_sport_instructor" value="0">&nbsp;&nbsp;Requiere de instructor
														</label>
														<label>
															<input type="checkbox" name="level_sport_no_instructor" id="level_sport_no_instructor" value="0">&nbsp;&nbsp;Solo requiere un horario especifico para ejercitarce solo
														</label>
													</div>
												</div>
											</div>
											<!-- Nivel 2/Deporte-->

											<div class="box-body">
												<div class="col-md-6">
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<h3 class="box-title">NIVEL REHABILITACIÓN</h3>
													</div>
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<label>
															<input type="checkbox" name="requires_instructor_help" id="requires_instructor_help" value="0">&nbsp;&nbsp;Requiere ayuda de instructor
														</label><br>
														<label>
															<input type="checkbox" name="requires_only_supervision" id="requires_only_supervision" value="0">&nbsp;&nbsp;Requiere solo supervisión
														</label><br>
														<label>
															<input type="checkbox" name="type_rehabilitation" id="type_rehabilitation" value="0">&nbsp;&nbsp;Que tipo de rehabilitación requiere (Agregar informe médico)
														</label>
														<label>
															<input type="checkbox" name="medical_report" id="medical_report" value="0">&nbsp;&nbsp;Entregado (informe médico)
														</label>
													</div>
												</div>
												<div class="col-md-6">
													<div class="form-group col-lg-12 col-md-12 col-xs-12">
														<h3 class="box-title">HORARIO</h3>
													</div>
													<div class="form-group col-md-12">
														<div class="form-horizonal">
															<div class="form-group row">
																<label for="monday_schedule" class="col-sm-2 control-label">
																	LUNES
																</label>
																<!-- <div class="col-md-1">
																	<input type="checkbox" name="monday_schedule" id="monday_schedule" value="0">
																</div> -->
																<div class="col-sm-10">
																	<select name="monday_time" id="monday_time" class="form-control select2" style="width: 100%">
																	</select>
																</div>
															</div>
															<div class="form-group row">
																<label for="tuesday_schedule" class="col-sm-2 control-label">
																	MARTES
																</label>
																<div class="col-sm-10">
																	<!-- <input type="checkbox" name="tuesday_schedule" id="tuesday_schedule" value="0"> -->
																	<select name="tuesday_time" id="tuesday_time" class="form-control select2" style="width: 100%">
																	</select>
																</div>
															</div>

															<div class="form-group row">
																<label for="wednesday_schedule" class="col-sm-2 control-label">
																	MIERCOLES
																</label>
																<div class="col-sm-10">
																	<!-- <input type="checkbox" name="wednesday_schedule" id="wednesday_schedule" value="0"> -->
																	<select name="wednesday_time" id="wednesday_time" class="form-control select2" style="width: 100%">
																	</select>
																</div>
															</div>

															<div class="form-group row">
																<label for="wednesday_time" class="col-sm-2 control-label">
																	JUEVES
																</label>
																<div class="col-sm-10">
																	<!-- <input type="checkbox" name="thursday_schedule" id="thursday_schedule" value="0"> -->
																	<select name="thursday_time" id="thursday_time" class="form-control select2" style="width: 100%">
																	</select>
																</div>
															</div>

															<div class="form-group row">
																<label for="friday_time" class="col-sm-2 control-label">
																	VIERNES
																</label>
																<div class="col-sm-10">
																	<!-- <input type="checkbox" name="friday_schedule" id="friday_schedule" value="0"> -->
																	<select name="friday_time" id="friday_time" class="form-control select2" style="width: 100%">
																	</select>
																</div>
															</div>
															<div class="form-group row">
																<label for="saturday_time" class="col-sm-2 control-label">
																	SÁBADO
																</label>
																<div class="col-sm-10">
																	<!-- <input type="checkbox" name="friday_schedule" id="friday_schedule" value="0"> -->
																	<select name="saturday_time" id="saturday_time" class="form-control select2" style="width: 100%">
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div> <!-- /.box -->
									</div>
									<div id="to-print" class="col-md-12 hidden">
										<div class="box box-solid">
											<div class="box-header with-border text-center">
												<h3 class="box-title"><strong>OBSERVACIONES IMPORTANTES</strong></h3>
											</div>
											<!-- /.box-header -->
											<div class="box-body">
												<ul>
													<li>Esta solicitud debera ser firmada en caso de menores de edad por el padre, madre o tutor</li>
													<li>Para garantizar su lugar es necesario realizar su pago de inscripcion y la primer mensualidad</li>
													<li>El plazo para pagar será durante la primer semana del mes</li>
													<li>De no cubrir su mensualidad podria perder su plaza</li>
													<li>Padra darse de baja debera avisar al encargado una semana antes de que finalice el mes</li>
													<li>El Solicitante padre, madre, tutor deberá presentar un certificado médico, en el cual declara que el alumno(a) no padece de enfermedad infecto contagiosa,
														ni defecto físico alguno que impida la práctica de la natación.
													</li>
													<li>SU PAGO PODRA SER EN EFECTIVO O POR DEPOSITO DE CUENTA</li>
												</ul>
											</div>
											<!-- /.box-body -->
										</div>
										<div class="box box-solid">
											<div class="box-header with-border text-center">
												<h3 class="box-title"><strong>CONDICIONES DE INSCRIPCIÓN</strong></h3>
											</div>
											<!-- /.box-header -->
											<div class="box-body ">
												<ul>
													<li>El solicitante manifiesta conocer las condiciones y normas de uso de las instalaciones.</li>
													<li>Es obligatorio el uso de gorro de natación, y traje de baño y una vez afuera de la alberca debera usar calzado adecuado antiderrapante </li>
													<li>El club no se hace responsable de la pérdida de prendas y objetos de valor extraviados</li>
													<li>La cobertura del riesgo de accidentes derivado de la práctica en estas actividades no lleva implicito tener un seguro de accidentes,
														corriendo a cargo del solicitante los gastos médicos por lesión de accidente a través de la Seguridad Social o Seguros Privados.
													</li>
													<li>El solicitante firma bajo protesta de decir verdad y en aceptación de todas las condiciones y reglas expuestas</li>
												</ul>
											</div>
											<!-- /.box-body -->
											<div class="box-footer">
												<div class="clearfix"></div>
												<br>
												<div class="form-group col-md-8">
													<p class="pull-left">
														________________________________<br>
														<span>Nombre y firma</span>
													</p>
													<p class="pull-right">
														________________________________<br>
														<span>Fecha</span>
													</p>
												</div>
											</div>
										</div>

									</div>
									<!-- /.box -->
									<div class="form-group col-lg-12 col-md-12 col-sm-12 col-xs-12 no-print">
										<!-- <button type="button" id="btnPrint" name="btnPrint" onclick="printContent('formulario')" class="btn btn-info"><i class="fa fa-print"></i> Imprimir</button> -->
										<button class="btn btn-primary no-print" type="submit" id="btnGuardar"><i class="fa fa-save"></i> Guardar</button>
										<button class="btn btn-danger no-print" onclick="cancelarform()" type="button"><i class="fa fa-arrow-circle-left"></i> Cancelar</button>
									</div>
								</form>
							</div>

							<!--fin centro-->
						</div>
					</div>
				</div>
				<!-- /.box -->
			</section>
			<!-- /.content -->
		</div>
		<div class="modal fade " id="modal-payment" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" onclick="window.location.reload()" aria-label="Close">
							<span aria-hidden="true">×</span></button>
						<h4 id="modal-title" class="modal-title">Realizar Cobro</h4>
					</div>
					<div class="modal-body">
						<input type="hidden" name="days" id="days">
						<input type="hidden" name="price" id="price">
						<input type="hidden" name="student_id" id="student_id">
						<input type="hidden" name="group_id" id="group_id">
						<div class="form-group">
							<label for="group">Grupo:</label>
							<input type="text" class="form-control" name="group" id="group" readonly>
						</div>
						<div class="form-group">
							<label for="students">Alumno:</label>
							<input type="text" class="form-control" name="alumn" id="alumn" readonly>
							<div id="help-alumn" class="help-block"></div>
						</div>
						<div class="form-group">
							<label for="type">Tipo Cobro:</label>
							<input type="text" class="form-control" name="payment_type" id="payment_type" readonly>
						</div>
						<div id="mensualidades" class="form-group">
							<label for="type">Mensualidad:</label>
							<select name="months[]" id="months" class="form-control" style="width: 100%;" multiple>
								<?= $helper->form_select_months('months') ?>
							</select>
							<div id="help_month" class="help-block"></div>
						</div>
						<div class="form-group">
							<label for="type">Costo:</label>
							<input type="text" class="form-control" name="cost" id="cost" readonly>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" onclick="window.location.reload()">Cancelar</button>
						<button id="store" name="store" type="button" class="btn btn-primary">Cobrar</button>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
	<?php
	} else {
		require 'noacceso.php';
	}
	require 'footer.php';
	$rand = rand();
	?>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
	<script src="scripts/vista_grupo.js?=v<?= $rand ?>"></script>
	<!-- <script src="scripts/payments.js?v=<?= $rand ?>"></script> -->
	<script>
		$(function(e) {

		});
	</script>
<?php
}

ob_end_flush();
?>