var tabla;

$(function () {
  let students = $("#students");
  let groups = $("#group");
  let months = $("#months");

  groups.select2();
  students.select2();
  months.select2();

  $("#monthly_payment").hide();

  //Init all payments
  toList();

  //Init to load group
  let data_groups = [];
  getGroups()
    .then((response) => {
      data_groups.push({
        id: 0,
        text: "Selecciona un grupo",
      });
      $.each(response, function (idx, v) {
        data_groups.push({
          id: v.idgrupo,
          text: v.nombre,
        });
      });

      groups.select2({
        data: data_groups,
      });
    })
    .catch((error) => console.log(error));

  groups.on("select2:select", function (e) {
    let params = e.params.data;
    let data_students = [];

    students.empty();
    data_students.push({
      id: 0,
      text: "Selecciona un alumno",
    });

    getStudents(params.id)
      .then((response) => {
        $.each(response, function (idx, v) {
          data_students.push({
            id: v.id,
            text: v.name,
            address: v.address,
            email: v.email,
            grupo: v.grupo,
            grupo_id: v.grupo_id,
            is_active: v.is_active,
            lastname: v.lastname,
            name: v.name,
            phone: v.phone,
            monday: v.monday,
            tuesday: v.tuesday,
            wednesday: v.wednesday,
            thursday: v.thursday,
            friday: v.friday,
            total_days: v.total_days,
          });
        });

        students.select2({
          data: data_students,
        });
      })
      .catch((error) => console.log(error));
  });

  //Students select
  students.on("select2:select", function (e) {
    let params = e.params.data;
    let group_id = groups.select2().val();
    $("#student_id").val(params.id);
    $("#group_id").val(parseInt(params.grupo_id));
    $("#days").val(parseInt(params.total_days));
    validPaymentType(group_id, params.id)
      .then((response) => {
        let res = response;
        let _type = parseInt(res.data) === 0 ? "I" : "M";
        if (parseInt(res.data) === 0) {
          $("#type").val("I").change();
          $("#type").prop("disabled", true);
        } else {
          $("#type").val("M").change();
          $("#type").prop("disabled", true);
        }
        return getPriceList(_type, parseInt(params.total_days));
      })
      .then((result) => {
        console.log(result);
        $("#price").val(parseFloat(result.data));
        $("#amount").val(parseFloat(result.data));
      })
      .catch((error) => console.log(error));
  });

  let count = 0;
  let days = parseFloat($("#days").val());
  let total = 0;
  let monthNames = [];

  months
    .on("select2:select", function (e) {
      let month = e.params.data.id;
      let monthName = e.params.data.text;
      let isPayments = 0;
      let price = parseFloat($("#price").val());
      let student_id = $("#student_id").val();
      let group_id = $("#group_id").val();
      let subtotal = 0;
      count++;
      subtotal += count * price;
      $("#amount").val(subtotal.toFixed(2));
      $("#help_month").empty();
      validPayment(month, student_id, group_id)
        .then((response) => {
          if (response.data == 1) {
            $("#btn-store").prop("disabled", true);
            monthNames.push({
              id: parseInt(e.params.data.id),
              name: e.params.data.text,
            });

            if (monthNames.length > 0) {
              $("#help_month").append("Meses pagado(s): ");
              $.each(monthNames, function (idx, v) {
                $("#help_month").append(v.name, ", ");
              });
            } else {
              $("#help_month").empty();
            }
          } else {
            if (monthNames.length > 0) {
              $("#btn-store").prop("disabled", true);
              $("#help_month").append("Meses pagado(s): ");
              $.each(monthNames, function (idx, v) {
                $("#help_month").append(v.name, ", ");
              });
            } else {
              $("#btn-store").prop("disabled", false);
            }
          }
        })
        .catch((error) => {
          console.log(error);
        });
    })
    .on("select2:unselect", function (e) {
      let price = parseFloat($("#price").val());
      let subtotal = 0;
      count--;
      subtotal += (count == 0 ? 1 : count) * price;
      $("#amount").val(subtotal.toFixed(2));

      $("#help_month").empty();
      $.each(monthNames, function (idx, v) {
        if (v.id === parseInt(e.params.data.id)) {
          monthNames.splice(idx, 1);
          return false;
        }
      });
      if (monthNames.length > 0) {
        $("#help_month").append("Meses pagado(s): ");
        $.each(monthNames, function (idx, v) {
          $("#help_month").append(v.name, ", ");
        });
      } else {
        $("#store").prop("disabled", false);
      }
    });

  $("#type").on("change", function (e) {
    let type = $(this).val();
    if (type === "I") {
      $("#monthly_payment").hide();
    } else {
      $("#monthly_payment").show();
    }
  });

  $("#btn-store").on("click", function (e) {
    let group_id = $("#group").val();
    let student_id = $("#students").val();
    let price = $("#price").val();
    let id = $("#id").val();
    let months = $("#months").val();
    let _type = $("#type").val();
    let description = "";
    if (_type === "I") {
      description = "Pago de Inscripción";
    } else {
      description = "Pago de mensualidad";
      price = $("#amount").val();
    }

    if (_.isEmpty(group_id) || _.isEmpty(student_id) || amount == 0) {
      alert(
        `Es imporante elegir un grupo posterior un alumno y por ultimo ingresar la cantidad para poder continuar con el pago!`
      );
    } else {
      if (!_.isEmpty(id) && id > 0) {
        update(id, group_id, student_id, amount)
          .then((response) => {
            if (response.status === 1) {
              Swal.fire({
                confirmButtonText: "Aceptar",
                title: "Exito!",
                text: response.message,
                icon: "success",
              }).then(() => {
                window.location.reload();
              });
            } else {
              Swal.fire({
                confirmButtonText: "Aceptar",
                title: "Error!",
                text: response.message,
                icon: "error",
              });
            }
          })
          .catch((error) => console.log(error));
      } else {
        // let _form = $("#cash_register").serialize();
        let data = {
          id: id,
          group: group_id,
          students: student_id,
          amount: price,
          type: _type,
          description: description,
          months: months,
        };
        console.log(data);
        store(data)
          .then((response) => {
            if (response.status === 1) {
              let id = parseInt(response.data);
              tabla.ajax.reload();
              $("#months").empty();
              window.location.href = "../vistas/ticket.php?id=" + id;
            } else {
              bootbox.alert(response.message);
            }
          })
          .catch((error) => console.log(error));
      }
    }
  });
});

const printTicket = function (e) {
  let data = $(e).data("object");
  let id = $(e).data("id");
  window.location.href = "../vistas/ticket.php?id=" + id;
};

const editar = function (e) {
  let data = $(e).data("object");
  $("#id").val(data.id);
  $("#group").val(data.group_id).change();
  $("#students").val(data.student_id).change();
  $("#amount").val(data.amount);
  $("#modal-default").modal("show");
};

const cancel = function (e) {
  let data = $(e).data("object");
  let isMonthly = data.type;
  let group_id = parseInt(data.group_id);
  let student_id = parseInt(data.student_id);
  $("#id").val(data.id);

  bootbox.confirm({
    title: "¿Esta seguro de cacelar el pago?",
    message: "No podras revertir esto",
    buttons: {
      cancel: {
        label: '<i class="fa fa-times"></i> Cancelar',
      },
      confirm: {
        label: '<i class="fa fa-check"></i> Confirmar',
      },
    },
    callback: function (result) {
      let next = 0;
      if (result) {        
        if (isMonthly == "I") {
          validMonthlyPayment(student_id, group_id)
            .then((response) => {
              let exists = parseInt(response.data);
              if (exists === 1) {
                bootbox.confirm({
                  title: "¿Esta seguro de eliminar el pago?",
                  message:
                    "Actualmente el pago a eliminar es una inscripción y se han efectuado pagos de mensualidades, si desea continuar tambien se van a eliminar los pagos mensuales",
                  callback: function (res) {
                    if (res) {
                      patch(data.id, 1)
                        .then((response) => {
                          if (response.status === 1) {
                            bootbox.alert({
                              message: response.message,
                              callback: function () {
                                window.location.reload();
                              },
                            });
                          } else {
                            bootbox.alert(response.message);
                          }
                        })
                        .catch((error) => console.log(error));
                    }
                  },
                });
              } else {
                patch(data.id, 0)
                .then((response) => {
                  if (response.status === 1) {
                    bootbox.alert({
                      message: response.message,
                      callback: function () {
                        window.location.reload();
                      },
                    });
                  } else {
                    bootbox.alert(response.message);
                  }
                })
                .catch((error) => console.log(error));
              }
            })
            .catch((error) => {
              console.log(error);
              bootbox.alert(error.responseText);
              return;
            });
        } else {
          patch(data.id, 0)
            .then((response) => {
              if (response.status === 1) {
                bootbox.alert({
                  message: response.message,
                  callback: function () {
                    window.location.reload();
                  },
                });
              } else {
                bootbox.alert(response.message);
              }
            })
            .catch((error) => console.log(error));
        }
      }
    },
  });
};

const getGroups = function () {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/grupos.php?op=all",
      dataType: "json",
    })
      .done(resolve)
      .fail(reject);
  });
};

const getStudents = function (group_id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/alumnos.php?op=all",
      dataType: "json",
      data: { group_id: group_id },
    })
      .done(resolve)
      .fail(reject);
  });
};

const store = function (data) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=store",
      type: "post",
      dataType: "json",
      data: data,
    })
      .done(resolve)
      .fail(reject);
  });
};

const update = function (id, group_id, student_id, amount, months = []) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=update",
      type: "post",
      dataType: "json",
      data: {
        id: id,
        group_id: group_id,
        student_id: student_id,
        amount: amount,
        months: months,
      },
    })
      .done(resolve)
      .fail(reject);
  });
};

const patch = function (id, references = 0) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=cancel",
      type: "get",
      dataType: "json",
      data: { id: id, references: references },
    })
      .done(resolve)
      .fail(reject);
  });
};

const detail = function (id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=detail",
      dataType: "json",
      data: { id: id },
    })
      .done(resolve)
      .fail(reject);
  });
};

const getAll = function () {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=all",
      dataType: "json",
    })
      .done(resolve)
      .fail(reject);
  });
};

const toList = function () {
  tabla = $("#tbllistado")
    .dataTable({
      aProcessing: true, //activamos el procedimiento del datatable
      aServerSide: true, //paginacion y filrado realizados por el server
      dom: "Bfrtip", //definimos los elementos del control de la tabla
      buttons: ["copyHtml5", "excelHtml5", "csvHtml5", "pdf"],
      ajax: {
        url: "../ajax/payments.php?op=all",
        type: "get",
        dataType: "json",
        error: function (e) {
          console.log(e.responseText);
        },
      },
      bDestroy: true,
      iDisplayLength: 10, //paginacion
      order: [[0, "desc"]], //ordenar (columna, orden)
    })
    .DataTable();
};

const validPaymentType = function (group_id, student_id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=valid_payment_type",
      dataType: "json",
      data: { group_id: group_id, student_id: student_id },
    })
      .done(resolve)
      .fail(reject);
  });
};

function getPriceList(type, days) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=price",
      type: "post",
      dataType: "json",
      data: { type: type, days: days },
    })
      .done(resolve)
      .fail(reject);
  });
}

function validPayment(month, student, group) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=valid_payment",
      type: "get",
      dataType: "json",
      data: { month: month, student_id: student, group_id: group },
    })
      .done(resolve)
      .fail(reject);
  });
}

function validMonthlyPayment(student, group) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=valid_monthly_payment",
      type: "get",
      dataType: "json",
      data: { student_id: student, group_id: group },
    })
      .done(resolve)
      .fail(reject);
  });
}
