var tabla;

//funcion que se ejecuta al inicio
function init() {
  let months = $("#months");
  let monday_time = $("#monday_time");
  let tuesday_time = $("#tuesday_time");
  let wednesday_time = $("#wednesday_time");
  let thursday_time = $("#thursday_time");
  let friday_time = $("#friday_time");
  let saturday_time = $("#saturday_time");

  monday_time.select2();
  tuesday_time.select2();
  wednesday_time.select2();
  thursday_time.select2();
  friday_time.select2();
  saturday_time.select2();
  months.select2();
  mostrarform(false);
  listar();

  schedules().then((response) => {
    let data = [];

    data.push({
      id: 0,
      text: "Selecciona un horario",
    });
    $.each(response, function (idx, v) {
      data.push({
        id: v.id,
        text: v.hora,
      });
    });

    monday_time.select2({
      data: data,
    });
    tuesday_time.select2({
      data: data,
    });
    wednesday_time.select2({
      data: data,
    });
    thursday_time.select2({
      data: data,
    });
    friday_time.select2({
      data: data,
	});
	saturday_time.select2({
		data: data,
	})
  });

  $("#formulario").on("submit", function (e) {
    guardaryeditar(e);
  });

  //cargamos los items al celect categoria
  $("#imagenmuestra").hide();

  //Validamos los checkbox
  $("#swimming_level_0_1").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#swimming_level_0_2").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#swimming_level_1").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#swimming_level_2").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#level_sport_instructor").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#level_sport_no_instructor").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#requires_instructor_help").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#requires_only_supervision").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#type_rehabilitation").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#medical_report").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#monday_schedule").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#tuesday_schedule").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#wednesday_schedule").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#thursday_schedule").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  $("#friday_schedule").on("change", function (e) {
    let isChecked = $(this).is(":checked");
    if (isChecked) {
      $(this).prop("value", 1);
    } else {
      $(this).prop("value", 0);
    }
  });

  let count = 0;
  let days = parseFloat($("#days").val());
  let total = 0;
  let monthNames = [];

  months
    .on("select2:select", function (e) {
      let month = e.params.data.id;
      let monthName = e.params.data.text;
      let isPayments = 0;
      let price = parseFloat($("#price").val());
      let student_id = $("#student_id").val();
      let group_id = $("#group_id").val();
      let subtotal = 0;
      count++;
      subtotal += count * price;
      $("#cost").val(subtotal.toFixed(2));
      $("#help_month").empty();
      validPayment(month, student_id, group_id)
        .then((response) => {
          if (response.data == 1) {
            $("#store").prop("disabled", true);
            monthNames.push({
              id: parseInt(e.params.data.id),
              name: e.params.data.text,
            });

            if (monthNames.length > 0) {
              $("#help_month").append("Meses pagado(s): ");
              $.each(monthNames, function (idx, v) {
                $("#help_month").append(v.name, ", ");
              });
            } else {
              $("#help_month").empty();
            }
          } else {
            if (monthNames.length > 0) {
              $("#store").prop("disabled", true);
              $("#help_month").append("Meses pagado(s): ");
              $.each(monthNames, function (idx, v) {
                $("#help_month").append(v.name, ", ");
              });
            } else {
              $("#store").prop("disabled", false);
            }
          }
        })
        .catch((error) => {
          console.log(error);
        });
    })
    .on("select2:unselect", function (e) {
      let price = parseFloat($("#price").val());
      let subtotal = 0;
      count--;
      subtotal += (count == 0 ? 1 : count) * price;
      $("#cost").val(subtotal.toFixed(2));

      $("#help_month").empty();
      $.each(monthNames, function (idx, v) {
        if (v.id === parseInt(e.params.data.id)) {
          monthNames.splice(idx, 1);
          return false;
        }
      });
      if (monthNames.length > 0) {
        $("#help_month").append("Meses pagado(s): ");
        $.each(monthNames, function (idx, v) {
          $("#help_month").append(v.name, ", ");
        });
      } else {
        $("#store").prop("disabled", false);
      }
    });

  //Store payment
  $("#store").on("click", function (e) {
    let group = $(this).data("group");
    let alumn = $(this).data("alumn");
    let price = $(this).data("price");
    let type = $(this).data("type");
    let description = "";
    let months = $("#months").val();
    let id = 0;
    if (type == "I") {
      description = "Pago de Inscripción";
    } else {
      description = "Pago de Mensualidad";
      price = $("#cost").val();
      if (months.length <= 0) {
        alert("Debe seleccionar una mensualidad a pagar!");
      }
    }
    let data = {
      id: id,
      group: group,
      students: alumn,
      amount: price,
      type: type,
      description: description,
      months: months,
    };
    store_payment(data)
      .then((response) => {
        if (response.status === 1) {
          let id = parseInt(response.data);
          tabla.ajax.reload();
          $("#months").empty();
          window.location.href = "../vistas/ticket.php?id=" + id;
        } else {
          bootbox.alert(response.message);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  });
}

//funcion limpiar
function limpiar() {
  $("#codigo").val("");
  $("#nombre").val("");
  $("#descripcion").val("");
  $("#stock").val("");
  $("#imagenmuestra").attr("src", "");
  $("#imagenactual").val("");
  $("#print").hide();
  $("#idalumno").val("");
  $("#formulario")[0].reset();
}

//funcion mostrar formulario
function mostrarform(flag) {
  limpiar();
  if (flag) {
    $("#listadoregistros").hide();
    $("#formularioregistros").show();
    $("#btnGuardar").prop("disabled", false);
    $("#btnagregar").hide();
    $("#btnasistencia").hide();
    $("#btnconducta").hide();
    $("#btncalificaciones").hide();
    $("#btncursos").hide();
    $("#btnlistas").hide();
    $("#btnreporte").hide();
    $("#btngrupos").hide();
  } else {
    $("#listadoregistros").show();
    $("#formularioregistros").hide();
    $("#btnagregar").show();
    $("#btnasistencia").show();
    $("#btnconducta").show();
    $("#btncalificaciones").show();
    $("#btncursos").show();
    $("#btnlistas").show();
    $("#btnreporte").show();
    $("#btngrupos").show();
  }
}

//cancelar form
function cancelarform() {
  limpiar();
  mostrarform(false);
}

//funcion listar
function listar() {
  var team_id = $("#idgrupo").val();
  tabla = $("#tbllistado")
    .dataTable({
      aProcessing: true, //activamos el procedimiento del datatable
      aServerSide: true, //paginacion y filrado realizados por el server
      dom: "Bfrtip", //definimos los elementos del control de la tabla
      buttons: [
        {
          extend: "copyHtml5",
          text: "Copiar",
          exportOptions: {
            columns: [0, ":visible"],
          },
        },
        {
          extend: "excelHtml5",
          exportOptions: {
            columns: ":visible",
          },
        },
        {
          extend: "pdfHtml5",
          exportOptions: {
            columns: [0, 1, 2, 5],
          },
        },
        {
          extend: "colvis",
          text: "Visor de columnas",
          collectionLayout: "fixed three-column",
        },
      ],
      ajax: {
        url: "../ajax/alumnos.php?op=listar",
        data: { idgrupo: team_id },
        type: "get",
        dataType: "json",
        error: function (e) {
          console.log(e.responseText);
        },
      },
      bDestroy: true,
      iDisplayLength: 10, //paginacion
      order: [[0, "desc"]], //ordenar (columna, orden)
    })
    .DataTable();
}

function printContent(divName) {
  let printContents = document.getElementById(divName).innerHTML;
  let originalContents = document.body.innerHTML;
  document.body.innerHTML = printContents;
  window.print();
  document.body.innerHTML = originalContents;
  window.location.reload();
}

//funcion para guardaryeditar
function guardaryeditar(e) {
  e.preventDefault(); //no se activara la accion predeterminada
  $("#btnGuardar").prop("disabled", true);
  let formData = new FormData($("#formulario")[0]);
  $.ajax({
    url: "../ajax/alumnos.php?op=guardaryeditar",
    type: "POST",
    data: formData,
    contentType: false,
    processData: false,
    success: function (datos) {
	  let r = JSON.parse(datos);
      if (r.status === 1) {
		limpiar();
        bootbox.alert({
          message: r.message,
          callback: function () {
            let res = JSON.parse(r.data);
            tabla.ajax.reload();
            window.location.href = `printRegistration.php?id=${res.id}`;
          },
        });
      } else {
        bootbox.alert({
          message: r.message,
          callback: function () {},
        });
      }
    },
  });
}

function modalPayment(element) {
  let team_id = $(element).data("team");
  let alumn_id = element.id;
  let alumn_name = $(element).data("alumn");
  let group_name = $(element).data("group");
  let payment_type = $(element).data("payment");
  let cost = $(element).data("price");
  let code_payment_type = parseInt(payment_type) === 0 ? "I" : "M";
  let total_days = parseInt($(element).data("days"));
  let monday = parseInt($(element).data("monday"));
  let tuesday = parseInt($(element).data("tuesday"));
  let wednesday = parseInt($(element).data("wednesday"));
  let thursday = parseInt($(element).data("thursday"));
  let friday = parseInt($(element).data("friday"));
  let show_days = `Horarios: `;

  if (monday > 0) {
    show_days += "Lunes ";
  }
  if (tuesday > 0) {
    show_days += " - Martes ";
  }
  if (wednesday > 0) {
    show_days += " - Miercoles ";
  }
  if (thursday > 0) {
    show_days += " - Jueves ";
  }
  if (friday > 0) {
    show_days += " - Viernes";
  }

  cost = parseFloat(cost);
  $("#days").val(0);
  $("#price").val(0);
  $("#modal-title").empty();
  $("#group").empty();
  $("#alumn").empty();
  $("#payment_type").empty();
  $("#cost").val(0);
  $("#help-alumn").empty();
  $("#cost").prop("readonly", true);
  $("#group").val(group_name);
  $("#alumn").val(alumn_name);
  $("#help-alumn").html(show_days);
  $("#days").val(total_days);
  $("#student_id").val(alumn_id);
  $("#group_id").val(team_id);
  if (parseInt(payment_type) === 0) {
    $("#mensualidades").hide();
    $("#modal-title").html("Cobro de Inscripción");
    $("#payment_type").val("Inscripción");
    $("#store").prop("disabled", false);
  } else {
    $("#mensualidades").show();
    $("#modal-title").html("Cobro de Mensualidad");
    $("#payment_type").val("Mensualidad");
    if (total_days >= 4) {
      $("#cost").prop("readonly", false);
    }
    $("#store").prop("disabled", true);
  }
  $("#cost").val(cost.toFixed(2));
  $("#price").val(cost);
  $("#store").attr("data-group", team_id);
  $("#store").attr("data-alumn", alumn_id);
  $("#store").attr("data-price", cost);
  $("#store").attr("data-type", code_payment_type);
  $("#modal-payment").modal("show");
}

function mostrar(id) {
  $.post("../ajax/alumnos.php?op=mostrar", { idalumno: id }, function (
    data,
    status
  ) {
	data = JSON.parse(data);
    mostrarform(true);
    let monday_time = $("#monday_time");
    let tuesday_time = $("#tuesday_time");
    let wednesday_time = $("#wednesday_time");
    let thursday_time = $("#thursday_time");
	let friday_time = $("#friday_time");
	let saturday_time = $("#saturday_time");

	monday_time.val(data.monday_schedule).trigger('change');
	tuesday_time.val(data.tuesday_schedule).trigger('change');
	wednesday_time.val(data.wednesday_schedule).trigger('change');
	thursday_time.val(data.thursday_schedule).trigger('change');
	friday_time.val(data.friday_schedule).trigger('change');
	saturday_time.val(data.saturday_schedule).trigger('change');
	$("#id").val(data.id);
    $("#nombre").val(data.name);
    $("#apellidos").val(data.lastname);
    $("#email").val(data.email);
    $("#phone").val(data.phone);
    $("#landline").val(data.landline);
    $("#age").val(data.age);
    if (data.is_disability == 1) {
      $("#is_disability").val(1);
      $("#is_disability").prop("checked", 1);
    } else {
      $("#no_disability").val(0);
      $("#no_disability").prop("checked", 1);
    }
    $("#describe_disability").val(data.describe_disability);

    if (data.recent_injury == 1) {
      $("#is_recente_injury").val(1);
      $("#is_recente_injury").prop("checked", 1);
    } else {
      $("#no_recente_injury").val(0);
      $("#no_recente_injury").prop("checked", 1);
    }
    $("#describe_injury").val(data.describe_injury);
    switch (parseInt(data.health_condition)) {
      case 1:
        $("#good_health_condition").val(1);
        $("#good_health_condition").prop("checked", 1);
        break;
      case 2:
        $("#regular_health_condition").val(2);
        $("#regular_health_condition").prop("checked", 1);
        break;

      case 3:
        $("#disease_health_condition").val(3);
        $("#disease_health_condition").prop("checked", 1);
        break;
    }

    $("#describe_health_condition").val(data.describe_health_condition);
    $("#tutor_name").val(data.tutor_name);
    $("#tutor_last_name").val(data.tutor_last_name);
    $("#address").val(data.tutor_address);
    $("#tutor_address").val(data.tutor_address);
    $("#suburb").val(data.suburb);
    $("#city").val(data.city);
    $("#municipality").val(data.municipality);
    $("#tutor_phone").val(data.tutor_phone);
    $("#tutor_landline").val(data.tutor_landline);
    $("#swimming_level_0_1").val(data.swimming_level_0_1);
    if (data.swimming_level_0_1 == 1) {
      $("#swimming_level_0_1").prop("checked", 1);
    }
    $("#swimming_level_0_2").val(data.swimming_level_0_2);
    if (data.swimming_level_0_2 == 1) {
      $("#swimming_level_0_2").prop("checked", 1);
    }
    $("#swimming_level_1").val(data.swimming_level_1);
    if (data.swimming_level_1 == 1) {
      $("#swimming_level_1").prop("checked", 1);
    }
    $("#swimming_level_2").val(data.swimming_level_2);
    if (data.swimming_level_2 == 1) {
      $("#swimming_level_2").prop("checked", 1);
    }
    $("#level_sport_instructor").val(data.level_sport_instructor);
    if (data.level_sport_instructor == 1) {
      $("#level_sport_instructor").prop("checked", 1);
    }
    $("#level_sport_no_instructor").val(data.level_sport_no_instructor);
    if (data.level_sport_no_instructor == 1) {
      $("#level_sport_no_instructor").prop("checked", 1);
    }
    $("#requires_instructor_help").val(data.requires_instructor_help);
    if (data.requires_instructor_help == 1) {
      $("#requires_instructor_help").prop("checked", 1);
    }
    $("#requires_only_supervision").val(data.requires_only_supervision);
    if (data.requires_only_supervision == 1) {
      $("#requires_only_supervision").prop("checked", true);
    }
    $("#type_rehabilitation").val(data.type_rehabilitation);
    if (data.type_rehabilitation == 1) {
      $("#type_rehabilitation").prop("checked", 1);
    }
    $("#medical_report").val(data.medical_report);
    if (data.medical_report == 1) {
      $("#medical_report").prop("checked", 1);
    }
    // $("#monday_schedule").val(data.monday_schedule);
    // if (data.monday_schedule == 1) {
    //   $("#monday_schedule").prop("checked", 1);
    // }
    // $("#tuesday_schedule").val(data.tuesday_schedule);
    // if (data.tuesday_schedule == 1) {
    //   $("#tuesday_schedule").prop("checked", 1);
    // }
    // $("#wednesday_schedule").val(data.wednesday_schedule);
    // if (data.wednesday_schedule == 1) {
    //   $("#wednesday_schedule").prop("checked", 1);
    // }
    // $("#thursday_schedule").val(data.thursday_schedule).change();
    // if (data.thursday_schedule == 1) {
    //   $("#thursday_schedule").prop("checked", 1);
    // }
    // $("#friday_schedule").val(data.friday_schedule).change();
    // if (data.friday_schedule == 1) {
    //   $("#friday_schedule").prop("checked", 1);
    // }
    $("#requires_only_supervision").val(data.requires_only_supervision);
    if (data.requires_only_supervision == 1) {
      $("#requires_only_supervision").prop("checked", 1);
    }
    $("#medical_report").val(data.medical_report);
    if (data.medical_report == 1) {
      $("#medical_report").prop("checked", 1);
    }
    $("#idalumno").val(data.id);
  });
}

//funcion para desactivar
function desactivar(idalumno) {
  bootbox.confirm("¿Esta seguro de desactivar este dato?", function (result) {
    if (result) {
      $.post(
        "../ajax/alumnos.php?op=desactivar",
        { idalumno: idalumno },
        function (e) {
          bootbox.alert(e);
          tabla.ajax.reload();
        }
      );
    }
  });
}

function activar(idalumno) {
  bootbox.confirm("¿Esta seguro de activar este dato?", function (result) {
    if (result) {
      $.post(
        "../ajax/alumnos.php?op=activar",
        { idalumno: idalumno },
        function (e) {
          bootbox.alert(e);
          tabla.ajax.reload();
        }
      );
    }
  });
}

function store_payment(data) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=store",
      type: "post",
      dataType: "json",
      data: data,
    })
      .done(resolve)
      .fail(reject);
  });
}

function get_payment_by_id(id) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=detail",
      type: "post",
      dataType: "json",
      data: { id: id },
    })
      .done(resolve)
      .fail(reject);
  });
}

function validPayment(month, student, group) {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/payments.php?op=valid_payment",
      type: "get",
      dataType: "json",
      data: { month: month, student_id: student, group_id: group },
    })
      .done(resolve)
      .fail(reject);
  });
}

function schedules() {
  return new Promise((resolve, reject) => {
    $.ajax({
      url: "../ajax/alumnos.php?op=schedules",
      type: "get",
      dataType: "json",
    })
      .done(resolve)
      .fail(reject);
  });
}

init();
