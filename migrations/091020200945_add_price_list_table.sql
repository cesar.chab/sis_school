CREATE TABLE `price_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` char(1) NOT NULL DEFAULT 'M',
  `days_per_week` int(11) NOT NULL DEFAULT 0,
  `amount` decimal(18,2) NOT NULL DEFAULT 0.00,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;


INSERT INTO sis_school.price_list (`type`,days_per_week,amount) VALUES 
('I',0,130.00)
,('M',2,380.00)
,('M',3,480.00)
;