<?php
require_once "../modelos/Alumnos.php";
require_once "../modelos/Payments.php";
if (strlen(session_id()) < 1)
	session_start();

$alumnos = new Alumnos();
$payments = new Payments();

$id = isset($_POST["idalumno"]) ? limpiarCadena($_POST["idalumno"]) : "";
$user_id = $_SESSION["idusuario"];

switch ($_GET["op"]) {
	case 'guardaryeditar':

		// if (!file_exists($_FILES['imagen']['tmp_name']) || !is_uploaded_file($_FILES['imagen']['tmp_name'])) {
		// 	$image = null; //$_POST["imagenactual"];
		// } else {
		// 	$ext = explode(".", $_FILES["imagen"]["name"]);
		// 	if ($_FILES['imagen']['type'] == "image/jpg" || $_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png") {
		// 		$image = round(microtime(true)) . '.' . end($ext);
		// 		move_uploaded_file($_FILES["imagen"]["tmp_name"], "../files/articulos/" . $image);
		// 	}
		// }
		//if (empty($id)) {
			$rspta = $alumnos->insertar($_POST);
			if ($rspta > 0) {
				echo json_encode(array(
					'status' => 1,
					'message' => 'Datos registrados correctamente',
					'data' => json_encode($alumnos->mostrar($rspta))
				));
			} else {
				echo json_encode(array(
					'status' => 0,
					'message' => 'No se puede registrar los datos',
					'data' => array()
				));
			}		
		/*} else {

			echo "<pre>";
			print_r($_POST);
			exit();
			$rspta = $alumnos->editar($id, $image, $name, $lastname, $email, $address, $phone, $c1_fullname, $c1_address, $c1_phone, $c1_note, $user_id, $team_id);
			echo $rspta ? "Datos actualizados correctamente" : "No se pudo actualizar los datos";
		}*/
		break;


	case 'desactivar':
		$rspta = $alumnos->desactivar($id);
		echo $rspta ? "Datos desactivados correctamente" : "No se pudo desactivar los datos";
		break;
	case 'activar':
		$rspta = $alumnos->activar($id);
		echo $rspta ? "Datos activados correctamente" : "No se pudo activar los datos";
		break;

	case 'mostrar':
		$rspta = $alumnos->mostrar($id);
		echo json_encode($rspta);
		break;


	case 'listar':
		$team_id = $_REQUEST["idgrupo"];
		$rspta = $alumnos->listar($user_id, $team_id);
		$data = array();
		
		while ($reg = $rspta->fetch_object()) {
			$reg->image = !empty($reg->image) ? $reg->image : "user.png";
			$typePayment = $payments->validPayments($team_id, $reg->id);
			$cost = 0;
			if ($typePayment == 0) {
				$cost = $payments->paymentCostByType('I', 0);
			} else {
				$cost = $payments->paymentCostByType('M', $reg->total_days);
			}

			$data[] = array(
				"0" => ($reg->is_active) ? '<button class="btn btn-warning btn-xs" onclick="mostrar(' . $reg->id . ')"><i class="fa fa-pencil"></i></button>' .
				 ' ' . '<a class="btn btn-info btn-xs" title="Imprimir inscripción" target="_blank" href="printRegistration.php?id='.$reg->id.'"><i class="fa fa-print"></i></a>' . 
				 ' ' . '<button class="btn btn-primary btn-xs" title="Cobro" data-monday='.$reg->monday.' data-tuesday='.$reg->tuesday.' data-wednesday='.$reg->wednesday.' data-thursday='.$reg->thursday.' data-friday='.$reg->friday.' data-days='.$reg->total_days.' data-price='.$cost.' data-payment='.$typePayment.' data-alumn='.json_encode($reg->name . ' ' . $reg->lastname).' data-group='.json_encode($reg->grupo).' id='.$reg->id.' data-team='.$team_id.' onclick="modalPayment(this)"><i class="fa fa-money"></i></button>' . ' ' . '<button class="btn btn-danger btn-xs" onclick="desactivar(' . $reg->id . ')"><i class="fa fa-close"></i></button>' : '<button class="btn btn-primary btn-xs" onclick="activar(' . $reg->id . ')"><i class="fa fa-check"></i></button>',
				//"1" => "<img src='../files/alumnos/" . $reg->image . "' height='50px' width='50px'>",
				"1" => $reg->name,
				"2" => $reg->lastname,
				"3" => $reg->phone,
				"4" => $reg->address,
				"5" => $reg->email
			);
		}
		$results = array(
			"sEcho" => 1, //info para datatables
			"iTotalRecords" => count($data), //enviamos el total de registros al datatable
			"iTotalDisplayRecords" => count($data), //enviamos el total de registros a visualizar
			"aaData" => $data
		);
		echo json_encode($results);
		break;
	case "all":
		$team_id = $_REQUEST["group_id"];
		$result = $alumnos->getAll($team_id);
		$resultSet = array();
		while ($cRecord = $result->fetch_assoc()) {
			$resultSet[] = $cRecord;
		}
		echo json_encode($resultSet);
	break;
	case "schedules": 
		$result = $alumnos->getSchedules();
		$resultSet = array();
		while ($cRecord = $result->fetch_assoc()) {
			$resultSet[] = $cRecord;
		}
		echo json_encode($resultSet);
	break;
}
