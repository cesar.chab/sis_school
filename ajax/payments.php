<?php
require_once "../modelos/Payments.php";
if (strlen(session_id()) < 1)
    session_start();

$payment = new Payments();

$op = $_REQUEST['op'];
$group_id = isset($_REQUEST['group']) ? $_REQUEST['group'] : null;
$student_id = isset($_REQUEST['students']) ? $_REQUEST['students'] : null;
$amount = isset($_REQUEST['amount']) ? $_REQUEST['amount'] : null;
$id = isset($_REQUEST['id']) ? $_REQUEST['id'] : 0;
$months = isset($_REQUEST['months']) ? $_REQUEST['months'] : array();
$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : null;
$message = isset($_REQUEST['description']) ? $_REQUEST['description'] : null;

switch ($_GET["op"]) {
    case "price":
        $type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'M';
        $days = isset($_REQUEST['days']) ? $_REQUEST['days'] : 2;
        $result = $payment->paymentCostByType($type, $days);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Precio Lista!',
            'data' => $result
        ));
    break;
    case 'valid_monthly_payment' :
        $student_id = $_REQUEST['student_id'];
        $group_id = $_REQUEST['group_id'];
        $result = $payment->isMonthlyPayment($student_id, $group_id);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Validando pago efectuado',
            'data' => $result
        ));
    break;
    case 'valid_payment_type': 
        $student_id = $_REQUEST['student_id'];
        $group_id = $_REQUEST['group_id'];
        $result = $payment->validPayments($group_id, $student_id);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Validando pago efectuado',
            'data' => $result
        ));
    break;
    case 'valid_payment': 
        $month = $_REQUEST['month'];
        $student_id = $_REQUEST['student_id'];
        $group_id = $_REQUEST['group_id'];
        $result = $payment->validMonthPayment($month, $student_id, $group_id);
        echo json_encode(array(
            'status' => 1,
            'message' => 'Validando pago efectuado',
            'data' => $result
        ));
    break;
    case "all":
        $result = $payment->all();
        while ($r = $result->fetch_object()) {
            $isDelete = ""; //($r->type == 'I') ? "hidden" : "";
            //<button data-object='" . json_encode($r) . "' onclick='editar(this)' class='btn btn-warning btn-xs btn-edit-payment'><i class='fa fa-edit'></i></button>
            $data[] = array(
                "0" => " <button data-id='".$r->id."' data-object='" . json_encode($r) . "' onclick='printTicket(this)' class='btn btn-info btn-xs'><i class='fa fa-print'></i></button> <button data-object='" . json_encode($r) . "' onclick='cancel(this)' class='btn btn-danger btn-xs ".$isDelete."'><i class='fa fa-close'></i></button>",
                "1" => $r->updated_at,
                "2" => $r->grupo,
                "3" => $r->alumno,
                "4" => $r->phone,
                "5" => $r->description,
                "6" => number_format($r->amount, 2),
                "7" => 'Efectivo',
                "8" => ($r->active) ? "<span class='label label-success'>Activo</span>" : "<span class=''>Activo</span>"
            );
        }
        $results = array(
            "sEcho" => 1, //info para datatables
            "iTotalRecords" => count($data), //enviamos el total de registros al datatable
            "iTotalDisplayRecords" => count($data), //enviamos el total de registros a visualizar
            "aaData" => $data
        );
        echo json_encode($results);
        break;
    case 'detail':
        $result = $payment->show($id);
        echo json_encode($result);
        break;
    case 'store':
        $result = $payment->store($group_id, $student_id, $amount, $type, $message, $months);
        if ($result) {
            echo json_encode(array(
                'status' => 1,
                'message' => 'Pago realizado correctamente!',
                'data' => $result
            ));
        } else {
            echo json_encode(array(
                'status' => 0,
                'message' => 'No se pudo realizar el pago, intent más tarde!',
                'data' => $result
            ));
        }
        break;
    case 'update':
        $result = $payment->update($id, $group_id, $student_id, $amount);
        if ($result) {
            echo json_encode(array(
                'status' => 1,
                'message' => 'pago actualizado correctamente!',
                'data' => $result
            ));
        } else {
            echo json_encode(array(
                'status' => 0,
                'message' => 'No se pudo actualizar el pago, intent más tarde!',
                'data' => $result
            ));
        }
        break;
    case 'cancel':
        $references = $_REQUEST['references'];
        $result = $payment->patch($id, $references);
        if ($result) {
            echo json_encode(array(
                'status' => 1,
                'message' => 'Pago eliminado correctamente!',
                'data' => $result
            ));
        } else {
            echo json_encode(array(
                'status' => 0,
                'message' => 'No se pudo eliminar el pago, intent más tarde!',
                'data' => $result
            ));
        }
        break;
    default:
        $result = $payment->all();
        $resultSet = array();
        while ($cRecord = $result->fetch_assoc()) {
            $resultSet[] = $cRecord;
        }
        echo json_encode($resultSet);
        break;
}
