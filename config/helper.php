<?php 
class Helper {
    
    //implementamos nuestro constructor
    public function __construct()
    {
    }

    function form_select_months($name, $selected = NULL)
		{
			$html = '';
			$meses  = array('', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre');
			for ($i = 1; $i <= 12; $i++) {
				if ($selected != $i) {
					$html .= '<option value="' . $i . '">' . $meses[$i] . '</option>';
				} else {
					$html .= '<option value="' . $i . '" selected="selected">' . $meses[$i] . '</option>';
				}
			}
			return $html;
		}
}
?>