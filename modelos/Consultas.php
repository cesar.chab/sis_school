<?php
//incluir la conexion de base de datos
require "../config/Conexion.php";
class Consultas
{


	//implementamos nuestro constructor
	public function __construct()
	{
	}

	//listar registros
	public function listar_asistencia($alumn_id, $team_id, $date_at)
	{

		$sql = "SELECT * FROM assistance a INNER JOIN alumn p ON a.alumn_id=p.id INNER JOIN team t ON a.team_id=t.idgrupo  WHERE a.alumn_id='$alumn_id' AND a.team_id='$team_id' AND a.date_at='$date_at'";
		return ejecutarConsulta($sql);
	}

	public function listar_comportamiento($alumn_id, $team_id, $date_at)
	{

		$sql = "SELECT * FROM behavior a INNER JOIN alumn p ON a.alumn_id=p.id INNER JOIN team t ON a.team_id=t.idgrupo  WHERE a.alumn_id='$alumn_id' AND a.team_id='$team_id' AND a.date_at='$date_at'";
		return ejecutarConsulta($sql);
	}

	public function ventasfechacliente($fecha_inicio, $fecha_fin, $idcliente)
	{
		$sql = "SELECT DATE(v.fecha_hora) as fecha, u.nombre as usuario, p.nombre as cliente, v.tipo_comprobante,v.serie_comprobante, v.num_comprobante , v.total_venta, v.impuesto, v.estado FROM venta v INNER JOIN persona p ON v.idcliente=p.idpersona INNER JOIN usuario u ON v.idusuario=u.idusuario WHERE DATE(v.fecha_hora)>='$fecha_inicio' AND DATE(v.fecha_hora)<='$fecha_fin' AND v.idcliente='$idcliente'";
		return ejecutarConsulta($sql);
	}

	public function totalcomprahoy()
	{
		$sql = "SELECT IFNULL(SUM(total_compra),0) as total_compra FROM ingreso WHERE DATE(fecha_hora)=curdate()";
		return ejecutarConsulta($sql);
	}

	public function totalventahoy()
	{
		$sql = "SELECT IFNULL(SUM(total_venta),0) as total_venta FROM venta WHERE DATE(fecha_hora)=curdate()";
		return ejecutarConsulta($sql);
	}

	public function comprasultimos_10dias()
	{
		$sql = " SELECT CONCAT(DAY(fecha_hora),'-',MONTH(fecha_hora)) AS fecha, SUM(total_compra) AS total FROM ingreso GROUP BY fecha_hora ORDER BY fecha_hora DESC LIMIT 0,10";
		return ejecutarConsulta($sql);
	}

	public function ventasultimos_12meses()
	{
		$sql = " SELECT DATE_FORMAT(fecha_hora,'%M') AS fecha, SUM(total_venta) AS total FROM venta GROUP BY MONTH(fecha_hora) ORDER BY fecha_hora DESC LIMIT 0,12";
		return ejecutarConsulta($sql);
	}

	public function cantidadalumnos($user_id)
	{
		$sql = "SELECT COUNT(*) total_alumnos FROM alumn WHERE user_id='$user_id'";


		return ejecutarConsulta($sql);
	}
	public function cantidadalumnos_porgrupo($user_id, $idgrupo)
	{
		//$sql = "SELECT a.id as idalumno, a.name,a.lastname,a.image  FROM alumn a 
		//INNER JOIN alumn_team alt ON a.id=alt.alumn_id WHERE a.user_id='$user_id' AND alt.team_id='$idgrupo'";
		$sql = "SELECT a.id AS idalumno, a.name, a.lastname, a.image, m.`time` AS monday_time, t.`time` AS tuesday_time, 
		w.`time` AS wednesday_time, th.`time` AS thursday_time, f.`time` AS friday_time, s.`time` AS saturday_time
		FROM alumn a 
		INNER JOIN alumn_team at on a.id = at.alumn_id 
		LEFT OUTER JOIN schedules m  ON a.monday_schedule = m.id
		LEFT OUTER JOIN schedules t  ON a.tuesday_schedule = t.id 
		LEFT OUTER JOIN schedules w  ON a.wednesday_schedule = w.id
		LEFT OUTER JOIN schedules th ON a.thursday_schedule = th.id 
		LEFT OUTER JOIN schedules f  ON a.friday_schedule = f.id 
		LEFT OUTER JOIN schedules s  ON a.saturday_schedule = s.id 
		WHERE a.user_id={$user_id} AND at.team_id={$idgrupo}";
		return ejecutarConsulta($sql);
	}
	public function cantidadg($user_id, $idgrupo)
	{
		$sql = "SELECT COUNT(*) total_alumnos  FROM alumn a INNER JOIN alumn_team alt ON a.id=alt.alumn_id WHERE a.user_id='$user_id' AND alt.team_id='$idgrupo'";


		return ejecutarConsulta($sql);
	}

	public function cantidadgrupos($idusuario)
	{
		$sql = "SELECT idgrupo,nombre, idusuario, favorito FROM team WHERE idusuario='$idusuario'";
		return ejecutarConsulta($sql);
	}

	public function cantidadarticulos()
	{
		$sql = "SELECT COUNT(*) totalar FROM articulo WHERE condicion=1";
		return ejecutarConsulta($sql);
	}
	public function totalstock()
	{
		$sql = "SELECT SUM(stock) AS totalstock FROM articulo";
		return ejecutarConsulta($sql);
	}

	public function cantidadcategorias()
	{
		$sql = "SELECT COUNT(*) totalca FROM categoria WHERE condicion=1";
		return ejecutarConsulta($sql);
	}

	public function getAllSchedules()
	{
		$sql = "SELECT * FROM schedules WHERE active=1";
		$result = ejecutarConsulta($sql);
		$resultSet = array();
		while ($cRecord = $result->fetch_assoc()) {
			$resultSet[] = $cRecord;
		}
		return $resultSet;
	}

	public function getAllStudents()
	{
		$today = date('d');
		$sql = "SELECT a.id AS idalumno, concat_ws(a.name, ' ',  a.lastname) as name, a.email, a.phone, a.image, 
		cast(m.`time` as datetime) as monday, 
		cast(t.`time` as datetime) as tuesday, 
		cast(w.`time` as datetime) as wednesday, 
		cast(th.`time` as datetime) as thursday, 
		cast(f.`time` as datetime) as friday, 
		cast(s.`time` as datetime) as saturday,
		dayname(now()) as today
		FROM alumn a 
		INNER JOIN alumn_team at on a.id = at.alumn_id 
		LEFT OUTER JOIN schedules m  ON a.monday_schedule = m.id
		LEFT OUTER JOIN schedules t  ON a.tuesday_schedule = t.id 
		LEFT OUTER JOIN schedules w  ON a.wednesday_schedule = w.id
		LEFT OUTER JOIN schedules th ON a.thursday_schedule = th.id 
		LEFT OUTER JOIN schedules f  ON a.friday_schedule = f.id 
		LEFT OUTER JOIN schedules s  ON a.saturday_schedule = s.id";
		$result = ejecutarConsulta($sql);
		$resultSet = array();
		while ($cRecord = $result->fetch_assoc()) {
			$resultSet[] = $cRecord;
		}
		$events = array();
		foreach ($resultSet as $key => $item) {
			$startDay = '';
			switch ($item['today']) {
				case 'Sunday':
					$startDay = date('Y-m-d H:i:s');					
					break;
				case 'Monday':
					$startDay = $item['monday'];
				break;
				case 'Tuesday':
					$startDay = $item['tuesday'];
				break;
				case 'Wednesday':
					$startDay = $item['wednesday'];
				break;
				case 'Thursday':
					$startDay = $item['thursday'];
				break;
				case 'Friday':
					$startDay = $item['friday'];
				break;
				case 'Saturday':
					$startDay = $item['saturday'];
				break;				
			}
			$aux = array(
				'id'=> $item['idalumno'],
				'title' => $item['name'],
				'name' => $item['name'],
				'start' => $startDay,
				'end' => $startDay,
				//'backgroundColor' => '#00c0ef', //Info (aqua)
				//'borderColor'    => '#00c0ef', //Info (aqua)
				'allDay' => false,
				'email' => $item['email'],
				'phone' => $item['phone']
			);
			array_push($events, $aux);
		}
	    return $events;
	}
}
