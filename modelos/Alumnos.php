<?php
//incluir la conexion de base de datos
require "../config/Conexion.php";
class Alumnos
{
	//implementamos nuestro constructor
	public function __construct()
	{
	}
	//metodo insertar regiustro
	public function insertar($data)
	{
		$user_id = $_SESSION["idusuario"];
		$created_at = date('Y-m-d H:i:s');
		$name = isset($data['nombre']) ? $data['nombre'] : "";
		$lastname = isset($data['apellidos']) ? $data['apellidos'] : "";
		$email = isset($data['email']) ? $data['email'] : "";
		$phone = isset($data['phone']) ? $data['phone'] : "";
		$landline = isset($data['landline']) ? $data['landline'] : "";
		$age = isset($data['age']) ? $data['age'] : 0;
		$is_disability = isset($data['isDisability']) ? $data['isDisability'] : 0;
		$describe_disability = isset($data['describe_disability']) ? $data['describe_disability'] : "";
		$recent_injury = isset($data['recent_injury']) ? $data['recent_injury'] : 0;
		$describe_injury = isset($data['describe_injury']) ? $data['describe_injury'] : "";
		$health_condition = isset($data['health_condition']) ? $data['health_condition'] : 0;
		$describe_health_condition = isset($data['describe_health_condition']) ? $data['describe_health_condition'] : "";
		$tutor_name = isset($data['tutor_name']) ? $data['tutor_name'] : "";
		$tutor_last_name = isset($data['tutor_last_name']) ? $data['tutor_last_name'] : "";
		$address = isset($data['address']) ? $data['address'] : "";
		$suburb = isset($data['suburb']) ? $data['suburb'] : "";
		$city = isset($data['city']) ? $data['city'] : "";
		$municipality = isset($data['municipality']) ? $data['municipality'] : "";
		$tutor_phone = isset($data['tutor_phone']) ? $data['tutor_phone'] : "";
		$tutor_landline = isset($data['tutor_landline']) ? $data['tutor_landline'] : '';
		$swimming_level_0_1 = isset($data['swimming_level_0_1']) ? $data['swimming_level_0_1'] : 0;
		$swimming_level_0_2 = isset($data['swimming_level_0_2']) ? $data['swimming_level_0_2'] : 0;
		$swimming_level_1 = isset($data['swimming_level_1']) ? $data['swimming_level_1'] : 0;
		$swimming_level_2 = isset($data['swimming_level_2']) ? $data['swimming_level_2'] : 0;
		$level_sport_instructor = isset($data['level_sport_instructor']) ? $data['level_sport_instructor'] : 0;
		$level_sport_no_instructor = isset($data['level_sport_no_instructor']) ? $data['level_sport_no_instructor'] : 0;
		$requires_instructor_help = isset($data['requires_instructor_help']) ? $data['requires_instructor_help'] : 0;
		$requires_only_supervision = isset($data['requires_only_supervision']) ? $data['requires_only_supervision'] : 0;
		$type_rehabilitation = isset($data['type_rehabilitation']) ? $data['type_rehabilitation'] : 0;
		$medical_report = isset($data['medical_report']) ? $data['medical_report'] : 0;

		$monday_schedule = (isset($data['monday_time']) && $data['monday_time'] != 0) ? $data['monday_time'] : 'NULL';
		$tuesday_schedule = (isset($data['tuesday_time']) && $data['tuesday_time'] != 0) ? $data['tuesday_time'] : 'NULL';
		$wednesday_schedule = (isset($data['wednesday_time']) && $data['wednesday_time'] != 0) ? $data['wednesday_time'] : 'NULL';
		$thursday_schedule = (isset($data['thursday_time']) && $data['thursday_time'] != 0) ? $data['thursday_time'] : 'NULL';
		$friday_schedule = (isset($data['friday_time']) && $data['friday_time'] != 0) ? $data['friday_time'] : 'NULL';
		$saturday_schedule = (isset($data['saturday_time']) && $data['saturday_time'] != 0) ? $data['saturday_time'] : 'NULL';
		$idgrupo = $data['idgrupo'];
		$idalumno = (isset($data['idalumno']) && !empty($data['idalumno'])) ? $data['idalumno'] : null;

		if (!empty($idalumno)) {
			$sql = "UPDATE alumn
			SET image=null, name='{$name}', lastname='{$lastname}', email='{$email}', address='{$address}', phone='{$phone}', user_id={$user_id}, landline='{$landline}', age={$age}, is_disability={$is_disability}, describe_disability='{$describe_disability}', 
			recent_injury={$recent_injury}, describe_injury='{$describe_injury}', health_condition={$health_condition}, describe_health_condition='{$describe_health_condition}', tutor_name='{$tutor_name}', tutor_last_name='{$tutor_last_name}', tutor_address='{$address}', suburb='{$suburb}', city='{$city}', municipality='{$municipality}', 
			tutor_phone='{$tutor_phone}', tutor_landline='{$tutor_landline}', swimming_level_0_1={$swimming_level_0_1}, swimming_level_1={$swimming_level_1}, swimming_level_2={$swimming_level_2}, level_sport_instructor={$level_sport_instructor}, level_sport_no_instructor={$level_sport_no_instructor}, 
			requires_instructor_help={$requires_instructor_help}, type_rehabilitation={$type_rehabilitation}, monday_schedule={$monday_schedule}, 
			tuesday_schedule={$tuesday_schedule}, wednesday_schedule={$wednesday_schedule}, thursday_schedule={$thursday_schedule}, friday_schedule={$friday_schedule}, 
			swimming_level_0_2={$swimming_level_0_2}, requires_only_supervision={$requires_only_supervision}, medical_report={$medical_report}, saturday_schedule={$saturday_schedule}
			WHERE id={$idalumno}";
			ejecutarConsulta($sql);
			$idalumno_new = $idalumno;
		} else {
			$sql = "INSERT INTO alumn (image, name, lastname, email, address, phone, created_at, user_id, landline, age, is_disability, describe_disability,
			recent_injury, describe_injury, health_condition, describe_health_condition, tutor_name, tutor_last_name, tutor_address, suburb, city, municipality, 
			tutor_phone, tutor_landline, swimming_level_0_1, swimming_level_1, swimming_level_2, level_sport_instructor, level_sport_no_instructor, requires_instructor_help, type_rehabilitation, monday_schedule, tuesday_schedule, wednesday_schedule, thursday_schedule, friday_schedule, swimming_level_0_2, requires_only_supervision, medical_report, saturday_schedule)
		   VALUES(null, '{$name}', '{$lastname}', '{$email}', '{$address}', '{$phone}', '{$created_at}', {$user_id}, '{$landline}', {$age}, {$is_disability}, '{$describe_disability}',
			{$recent_injury}, '{$describe_injury}', {$health_condition}, '{$describe_health_condition}', '{$tutor_name}', '{$tutor_last_name}', '{$address}', '{$suburb}', '{$city}', '{$municipality}', '{$tutor_phone}', 
			'{$tutor_landline}', {$swimming_level_0_1}, {$swimming_level_1}, {$swimming_level_2}, {$level_sport_instructor}, {$level_sport_no_instructor}, {$requires_instructor_help}, 
			{$type_rehabilitation}, {$monday_schedule}, {$tuesday_schedule}, {$wednesday_schedule}, {$thursday_schedule}, {$friday_schedule}, {$swimming_level_0_2}, {$requires_only_supervision}, {$medical_report}, {$saturday_schedule})";
		
			$idalumno_new = ejecutarConsulta_retornarID($sql);
			if ($idalumno_new > 0) {
				ejecutarConsulta("INSERT INTO alumn_team (alumn_id, team_id) VALUES({$idalumno_new}, {$idgrupo})");
			}
		}

		return $idalumno_new;
	}


	public function editar($id, $image, $name, $lastname, $email, $address, $phone, $c1_fullname, $c1_address, $c1_phone, $c1_note, $user_id)
	{
		$sql = "UPDATE alumn SET image='$image',name='$name', lastname='$lastname',email='$email',address='$address',phone='$phone' ,c1_fullname='$c1_fullname', c1_address='$c1_address', c1_phone='$c1_phone',c1_note='$c1_note',user_id='$user_id'
	WHERE id='$id'";
		return ejecutarConsulta($sql);
	}
	public function desactivar($id)
	{
		$sql = "UPDATE alumn SET is_active='0' WHERE id='$id'";
		return ejecutarConsulta($sql);
	}
	public function activar($id)
	{
		$sql = "UPDATE alumn SET is_active='1' WHERE id='$id'";
		return ejecutarConsulta($sql);
	}

	//metodo para mostrar registros
	public function mostrar($id)
	{
		$sql = "SELECT a.*, m.`time` AS monday_time, t.`time` AS tuesday_time, 
		w.`time` AS wednesday_time, th.`time` AS thursday_time, f.`time` AS friday_time, s.`time` as saturday_time
		FROM alumn a 
		LEFT OUTER JOIN schedules m  ON a.monday_schedule = m.id
		LEFT OUTER JOIN schedules t  ON a.tuesday_schedule = t.id 
		LEFT OUTER JOIN schedules w  ON a.wednesday_schedule = w.id
		LEFT OUTER JOIN schedules th ON  a.thursday_schedule = th.id 
		LEFT OUTER JOIN schedules f  ON a.friday_schedule = f.id 
		LEFT OUTER JOIN schedules s ON a.saturday_schedule = s.id 
		WHERE a.id={$id}";
		return ejecutarConsultaSimpleFila($sql);
	}

	//listar registros 
	public function listar($user_id, $team_id)
	{
		$sql = "SELECT a.id,a.image,a.name,a.lastname,a.email,a.address,a.phone,a.c1_fullname,a.c1_address,a.c1_phone,a.c1_note, a.is_active, 
			a.user_id,alt.team_id as grupo_id, t.nombre as grupo, (a.monday_schedule + a.tuesday_schedule + a.wednesday_schedule + a.thursday_schedule  + a.friday_schedule ) as total_days,
			a.monday_schedule as monday, a.tuesday_schedule as tuesday, a.wednesday_schedule as wednesday, a.thursday_schedule as thursday, a.friday_schedule as friday
			FROM alumn a INNER JOIN alumn_team alt ON a.id=alt.alumn_id 
			INNER JOIN team t on alt.team_id = t.idgrupo 
			WHERE a.user_id='$user_id' AND alt.team_id='$team_id' 
			ORDER BY a.id DESC ";
		return ejecutarConsulta($sql);
	}


	public function verficar_alumno($user_id, $team_id)
	{
		$sql = "SELECT * FROM alumn a INNER JOIN alumn_team alt ON a.id=alt.alumn_id WHERE a.is_active=1 AND a.user_id='$user_id' AND alt.team_id='$team_id' ORDER BY a.id DESC ";
		return ejecutarConsultaSimpleFila($sql);
	}
	public function listar_calif($user_id, $team_id)
	{
		$sql = "SELECT a.id AS idalumn,a.image,a.name,a.lastname,a.email,a.address,a.phone,a.c1_fullname,a.c1_address,a.c1_phone,a.c1_note, a.is_active, a.user_id FROM alumn a INNER JOIN alumn_team alt ON a.id=alt.alumn_id WHERE a.is_active=1 AND a.user_id='$user_id' AND alt.team_id='$team_id' ORDER BY a.id DESC ";
		return ejecutarConsulta($sql);
	}
	//listar registros activos

	//implementar un metodo para listar los activos, su ultimo precio y el stock(vamos unir con el ultimo registro de la tabla detalle_ingreso)

	/**
	 * 27/09/2020
	 * Implementation
	 */
	public function getAll($group_id)
	{
		$sql = "SELECT a.id,a.image,a.name,a.lastname,a.email,a.address,a.phone,a.c1_fullname,a.c1_address,a.c1_phone,a.c1_note, a.is_active, 
		a.user_id,alt.team_id as grupo_id, t.nombre as grupo, (a.monday_schedule + a.tuesday_schedule + a.wednesday_schedule + a.thursday_schedule  + a.friday_schedule ) as total_days,
		a.monday_schedule as monday, a.tuesday_schedule as tuesday, a.wednesday_schedule as wednesday, a.thursday_schedule as thursday, a.friday_schedule as friday
		FROM alumn a INNER JOIN alumn_team alt ON a.id=alt.alumn_id 
		INNER JOIN team t on alt.team_id = t.idgrupo WHERE a.is_active=1 AND alt.team_id={$group_id}";
		return ejecutarConsulta($sql);
	}

	public function getSchedules()
	{
		return ejecutarConsulta("SELECT id, time as hora FROM schedules WHERE active = 1");
	}
}
