<?php
//incluir la conexion de base de datos
require "../config/Conexion.php";
class Payments
{


    //implementamos nuestro constructor
    public function __construct()
    {
    }

    public function all(){
        $sql = "SELECT p.type, p.updated_at, p.id, p.amount, p.method, t.idgrupo as group_id, t.nombre AS grupo, a.id as student_id, concat(a.name , ' ', a.lastname ) AS alumno,  a.email, a.address , a.phone,
        u.nombre AS usuario, u.tipo_documento, u.num_documento, u.direccion, u.telefono, u.email AS correo, u.cargo, p.active, p.description
        FROM payments p 
        INNER JOIN team t ON p.group_id = t.idgrupo 
        INNER JOIN alumn a ON p.student_id = a.id 
        INNER JOIN usuario u ON p.user_id = u.idusuario WHERE p.active = 1";
        return ejecutarConsulta($sql);
    }

    /**
     * Function to get detail payments by id
     */
    public function show ($id) {
        $sql = "SELECT p.updated_at, p.id, p.amount, p.method, t.idgrupo as group_id, t.nombre AS grupo, a.id as student_id, concat(a.name , ' ', a.lastname ) AS alumno,  a.email, a.address , a.phone,
        u.nombre AS usuario, u.tipo_documento, u.num_documento, u.direccion, u.telefono, u.email AS correo, u.cargo, p.active, p.description
        FROM payments p 
        INNER JOIN team t ON p.group_id = t.idgrupo 
        INNER JOIN alumn a ON p.student_id = a.id 
        INNER JOIN usuario u ON p.user_id = u.idusuario WHERE p.id = {$id} ";
        return ejecutarConsultaSimpleFila($sql);
    }

    public function show_detail ($id) {
        $sql = "SELECT * FROM payment_detail WHERE payment_id = {$id}";
        $result =  ejecutarConsulta($sql);
        $aux = array();
        while ($r = $result->fetch_object()) {
            array_push($aux, $r);
        }
        return $aux;
    }

    /**
     * Method to add a charge
     */
    public function store($groupId, $studentId, $amount, $type, $description, $months = [])
    {
        $user_id = $_SESSION["idusuario"];
        $created_at = date('Y-m-d H:i:s');
        $year = date('Y');
        
        $sql = "INSERT INTO payments (group_id, student_id, amount, method, is_paid, user_id, created_at, updated_at, active, `type`, `description`, `year`) VALUES ({$groupId}, {$studentId}, {$amount}, 'cash', 1, {$user_id},'{$created_at}', '{$created_at}', 1, '{$type}', '{$description}', {$year})";
        $result = ejecutarConsulta_retornarID($sql);
        if (!empty($months) && $type == 'M') {
            if ($result > 0) {
                $price = floatval($amount) / sizeof($months);
                foreach ($months as $key => $m) {
                    $sqlI = "INSERT INTO payment_detail (payment_id, `month`, amount, `description`, `year`) VALUES({$result}, {$m}, {$price}, '{$description}', {$year})";
                    $resulI = ejecutarConsulta_retornarID($sqlI);
                }
            }
        }
        return json_encode($result);

    }

    /**
     * Method to add a charge
     */
    public function update($id, $groupId, $studentId, $amount)
    {
        $user_id = $_SESSION["idusuario"];
        $updated_at = date('Y-m-d H:i:s');
        $sql = "UPDATE payments SET group_id={$groupId}, student_id={$studentId}, amount={$amount}, `method`='cash', is_paid=1, user_id={$user_id}, updated_at='{$updated_at}' WHERE id={$id}";
        $result = ejecutarConsulta($sql);
        return json_encode($result);
    }

    public function patch ($id, $references) {
        
        $user_id = $_SESSION["idusuario"];
        $updated_at = date('Y-m-d H:i:s');

        $data = ejecutarConsultaSimpleFila("SELECT * FROM payments WHERE id={$id}");

        $sql = "UPDATE payments SET active=0, user_id={$user_id}, updated_at='{$updated_at}' WHERE id={$id}";
        $result = ejecutarConsulta($sql);

        if ($references == 1) {
            $cancelReferences = "UPDATE payments SET active=0, user_id={$user_id}, updated_at='{$updated_at}' WHERE student_id={$data['student_id']} AND group_id={$data['group_id']} AND active=1";
            $resultReferences = ejecutarConsulta($cancelReferences);
        }        
        return json_encode($result);
    }

    /**
     * Funcion para validar que paga realizar el alumno si es una inscripcion o una mensualidad
     */
    public function validPayments($grupo_id, $alumno_id) {
        $isInscription = 0;
        $sql = "SELECT count(*) as inscripcion FROM payments p WHERE p.active= 1 AND p.`type` = 'I' AND group_id = {$grupo_id} AND student_id  = {$alumno_id}";
        $result = ejecutarConsultaSimpleFila($sql);
        if (!empty($result)) {  
            if ($result['inscripcion'] > 0) {
                $isInscription = 1;
            }
        }
        return $isInscription;
    }

    /**
     * Funcion para obtener el precio de lista basado en el tipo de pago y dias de pago
     */
    public function paymentCostByType($type, $days = 0) {
        $cost = 0;
        if ($type == 'I') {
            $sql = "SELECT * FROM price_list WHERE type ='I' LIMIT 1";    
        } else if ($days == 1 && $type == 'M') {
            $sql = "SELECT * FROM price_list WHERE type ='M' ORDER BY days_per_week ASC LIMIT 1";    
        } else if ($days > 3 && $type == 'M') {
            $sql = "SELECT * FROM price_list WHERE type ='M' ORDER BY days_per_week DESC LIMIT 1";    
        } else {
            $sql = "SELECT * FROM price_list WHERE type ='{$type}' AND days_per_week = {$days}";
        }         
        $result = ejecutarConsultaSimpleFila($sql);
        if (!empty($result)) {
            $cost = $result['amount'];
        }
        return $cost;
    }

    /**
     * Funcion para validar si el mes seleccionado se ha pagado o no del año en curso
     */
    public function validMonthPayment($month, $student_id, $group_id) {
        $year = date('Y');
        $sql = "SELECT COUNT(*) as pagado FROM payment_detail pd 
        INNER JOIN payments p ON pd.payment_id  = p.id
        WHERE pd.month = {$month} AND pd.year = {$year} AND p.student_id = {$student_id} AND p.group_id={$group_id} AND p.active=1";
        $result = ejecutarConsultaSimpleFila($sql);
        if (!empty($result)) {
            if ($result['pagado'] > 0) {
                return 1;
            }
        }
        return 0;
    }

    /**
     * Funcion para determinar si el alumno tiene mensualidades vigentes 
     * esto es para validar al momento de eliminar un inscripcion
     */
    public function isMonthlyPayment ($student_id, $group_id) {
        $year = date('Y');
        $sql = "SELECT COUNT(*) AS total FROM payment_detail pd 
        INNER JOIN payments p ON pd.payment_id = p.id 
        WHERE p.student_id = {$student_id} AND p.group_id = {$group_id} AND p.active = 1 AND p.`year` = {$year}";
        $result = ejecutarConsultaSimpleFila($sql);
        if (!empty($result)) {
            if ($result['total'] > 0) {
                return 1;
            }
        }
        return 0;
    }

}
